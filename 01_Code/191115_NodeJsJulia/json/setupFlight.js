var setup=
{
	nodes: [
		{
			id: 'n0', 
			parent: '11',
			degrees_of_freedom:[0,1,2,3,4,5] ,
			restrained_degrees_of_freedom:[true,true,true,true,true,true],
			position: { x: 0, y: 0,z:0 },
			currPosition:new THREE.Vector3(0,0,0),
			force:{ x: 0, y: 10,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
            moment:{ x: 0, y: 0,z:0 },
            velocity:new THREE.Vector3(0,0,0),
			
		},
	],
	
	edges: [
        // { 
        //     id: 'e0', source: 0, target: 1 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
        //     currentRestLength:0,
        //     axis:Y_AXIS,
        //     pos2: new THREE.Vector3(0,0,0),
        //     angle1v: new THREE.Vector3(0,0,0),
        //     angle2v: new THREE.Vector3(0,0,0),
        //     angle1:new THREE.Quaternion(),
        //     angle2:new THREE.Quaternion(),
        //     currentTransverseArea:0,
        //     currentTransverseStrainSum:0

		// },
        
	],

	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 3*6,

	animation :  {
	
		showDisplacement : true,
		exaggeration : 10,
		speed:3.0
		
	},

	viz :  {
	
		
		minStress:-500,
		maxStress: 500,
		colorMaps:[YlGnBu, winter, coolwarm,jet],
		colorMap:0,
		
	},

	bar:false
	
};