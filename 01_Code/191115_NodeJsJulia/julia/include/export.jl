# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

function updateDataAndSave!(metavoxel,setup,fileName)
    nodes      = setup["nodes"]
    edges      = setup["edges"]
    
    setup["animation"]["showDisplacement"]=false
    voxCount=size(nodes)[1]
    linkCount=size(edges)[1]
    
    N_displacement=Array(metavoxel["N_displacementGPU"])
    N_position=Array(metavoxel["N_positionGPU"])
    N_angle=Array(metavoxel["N_angleGPU"])
    E_stress=Array(metavoxel["E_stressGPU"])
    
    setup["viz"]["maxStress"]=maximum(E_stress)
    setup["viz"]["minStress"]=minimum(E_stress)
    setup["animation"]["exaggeration"]=1.0

    


    i=1
	for edge in edges
        edge["stress"]=E_stress[i]
        i=i+1

    end

    scale1=1.0
    if haskey(setup,"scale")
        scale1=setup["scale"]
    end
    scale2=1.0
    scale=scale1/scale2

    if haskey(setup,"voxelSize")
        setup["voxelSize"]=setup["voxelSize"]*scale1
    end

    
    if haskey(setup,"supports")
        supports      = setup["supports"]
        for support in supports
            support[1]["min"]["x"]=support[1]["min"]["x"]*scale1
            support[1]["min"]["y"]=support[1]["min"]["y"]*scale1
            support[1]["min"]["z"]=support[1]["min"]["z"]*scale1
            support[1]["max"]["x"]=support[1]["max"]["x"]*scale1
            support[1]["max"]["y"]=support[1]["max"]["y"]*scale1
            support[1]["max"]["z"]=support[1]["max"]["z"]*scale1
        end
    end
    if haskey(setup,"loads")
        loads         = setup["loads"]
        for load in loads
            load[1]["min"]["x"]=load[1]["min"]["x"]*scale1
            load[1]["min"]["y"]=load[1]["min"]["y"]*scale1
            load[1]["min"]["z"]=load[1]["min"]["z"]*scale1
            load[1]["max"]["x"]=load[1]["max"]["x"]*scale1
            load[1]["max"]["y"]=load[1]["max"]["y"]*scale1
            load[1]["max"]["z"]=load[1]["max"]["z"]*scale1
        end
    end

    if haskey(setup,"materials")
        materials         = setup["materials"]
        for material in materials
            material[1]["min"]["x"]=material[1]["min"]["x"]*scale1
            material[1]["min"]["y"]=material[1]["min"]["y"]*scale1
            material[1]["min"]["z"]=material[1]["min"]["z"]*scale1
            material[1]["max"]["x"]=material[1]["max"]["x"]*scale1
            material[1]["max"]["y"]=material[1]["max"]["y"]*scale1
            material[1]["max"]["z"]=material[1]["max"]["z"]*scale1
        end
    end

    if haskey(setup,"fixedDisplacements")
        disps         = setup["fixedDisplacements"]
        for disp in disps
            disp[1]["min"]["x"]=disp[1]["min"]["x"]*scale1
            disp[1]["min"]["y"]=disp[1]["min"]["y"]*scale1
            disp[1]["min"]["z"]=disp[1]["min"]["z"]*scale1
            disp[1]["max"]["x"]=disp[1]["max"]["x"]*scale1
            disp[1]["max"]["y"]=disp[1]["max"]["y"]*scale1
            disp[1]["max"]["z"]=disp[1]["max"]["z"]*scale1
        end
    end
 
    i=1          
	for node in nodes
        node["posTimeSteps"]=[]
        node["angTimeSteps"]=[]
        node["degrees_of_freedom"]=""
        
        node["position"]["x"]= (N_position[i].x)*scale#-12.5
        node["position"]["y"]= (N_position[i].y)*scale
        node["position"]["z"]= (N_position[i].z)*scale#+30.0


        node["displacement"]["x"]= N_displacement[i].x*scale
        node["displacement"]["y"]= N_displacement[i].y*scale
        node["displacement"]["z"]= N_displacement[i].z*scale
        
        
        node["angle"]["x"]= N_angle[i].x
        node["angle"]["y"]= N_angle[i].y
        node["angle"]["z"]= N_angle[i].z

        i=i+1

    end

    
    # pass data as a json string (how it shall be displayed in a file)
    stringdata = JSON.json(setup)

    # write the file with the stringdata variable information
    open(fileName, "w") do f
            write(f, stringdata)
    end
    
end

#################################################

function getSetup(fileName)
    setup = Dict()
    # name=string("../json/setupTestUni$latticeSize",".json")
    name=string("./json/$(fileName)",".json")
     #open("../json/setupValid2.json", "r") do f
     #open("../json/setupTest.json", "r") do f
    # open("../json/trialJulia.json", "r") do f
     #open("../json/setupTestUni4.json", "r") do f
    # open("../json/setupChiral.json", "r") do f
     #open("../json/setupTestCubeUni10.json", "r") do f
    open(name, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end

    setup=setup["setup"]
    setup["viz"]["colorMaps"]=""
    return setup
end
