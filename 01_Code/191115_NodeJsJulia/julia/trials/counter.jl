using WebIO
# using IJulia
using JSExpr # you may need to install this package

# using Pkg
# Pkg.add("Mux")

using Mux


# function myapp(req)
#     return "<h1>Hello, $(req[:params][:user])!</h1>"
# end

# function username(app, req)
#     req[:params][:user] = req[:cookies][:user]
#     return app(req) # We could also alter the response, but don't want to here
# end

# username(myapp, req)

# mux(username, myapp)

# @app test = (
#   Mux.defaults,
#   page(respond("<h1>Hello World!</h1>")),
#   page("/about", probabilty(0.1, respond("<h1>Boo!</h1>")),respond("<h1>About Me</h1>")),
#   page("/user/:user", req -> "<h1>Hello, $(req[:params][:user])!</h1>"),Mux.notfound())

# serve(test)


function counter(start=0)
    println("sshshs")
    scope = Scope()

    # updates to this update the UI
    count = Observable(scope, "count", start)

    onjs(count, # listen on JavaScript
         JSExpr.@js x->this.dom.querySelector("#count").textContent = x)

    on(count) do n # listen on Julia
        println(n > 0 ? "+"^n : "-"^abs(n))
    end

    println("sjsjsjs")

    btn(label, d) = dom"button"(
        label,
        events=Dict(
            "click" => JSExpr.@js () -> $count[] = $count[] + $d
        )
    )

    println("sss")

    scope.dom = dom"div"(
        btn("increment", 1),
        btn("decrement", -1),
        dom"div#count"(string(count[])),
    )

    println("sjsjs")

    scope
end

# Display in whatever frontend is avalaible
function main()
    if @isdefined(IJulia) || @isdefined(Juno)
        println("hersssee")
        return counter(1)
    elseif @isdefined(Blink)
        println("heffggree")
        win = Window()
        body!(win, counter(1))
    elseif @isdefined(Mux)
        println("herwwwee")
        @sync webio_serve(page("/", req -> counter(1)), 8000)
    else
        println("hersssswwwee")
        error("do one of using Mux, using Blink before running the
               example, or run it from within IJulia or Juno")
    end
end

main()

while true;sleep(1);end


