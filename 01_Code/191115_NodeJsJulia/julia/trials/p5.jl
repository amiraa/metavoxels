# using WebIO
# using JSExpr
# # using Mux

# # function myapp(req)
# #     return node(:p, "Hello, Mux!")
# # end

# # webio_serve(page("/", req -> myapp(req)),8000)

# function hue_app()
#     scope = Scope()
#     import!(scope, ["//cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.7/p5.js"])

#     # Note: we explicitly qualify @js, @var, and @new with the JSExpr module
#     # because Blink.jl exports macros with the same name.
#     sketch = JSExpr.@js function (p5)
#         JSExpr.@var s = function(p)
#             JSExpr.@var barWidth = 20
#             JSExpr.@var lastBar = -1

#             p.setup = function ()
#                p.createCanvas(720, 400)
#                p.colorMode(p.HSB, p.height, p.height, p.height)
#                p.noStroke()
#                p.background(0)
#             end

#             p.draw = function ()
#                 JSExpr.@var whichBar = p.mouseX / barWidth
#                 JSExpr.@var barX = whichBar * barWidth;
#                 if whichBar != lastBar
#                    p.fill(p.mouseY, p.height, p.height)
#                    p.rect(barX, 0, barWidth, p.height)
#                    lastBar = whichBar
#                end
#             end
#         end
#         this.dom.querySelector("#p5container").innerText = "";
#         JSExpr.@new p5(s, "p5container");
#     end
#     onimport(scope, sketch)

#     scope.dom = dom"div#p5container"("Loading p5...")

#     scope
# end



# while true;sleep(1);end

using WebSockets, WebIO
using WebIO, JSExpr, JSCall, Interact
using Mux
function hue_app22()
    # import a javascript library
    # w = Scope(imports=["//cdnjs.cloudflare.com/ajax/libs/p5.js/0.5.11/p5.js"])
    w = Scope(imports=["//cdn.jsdelivr.net/npm/p5@1.0.0/lib/p5.js"])
    # Define a Julia function with the @js macro, that will get translated to javascript
    onimport(w, @js function (p5)
        
        function sketch(s)
            s.setup = () -> s.createCanvas(640, 200)
            s.draw = function ()
            if s.mouseIsPressed
                s.fill(0); s.stroke(255)
            else
                s.fill(255); s.stroke(0)
            end
            s.ellipse(s.mouseX, s.mouseY, 20, 20)
            end
        end
        @new p5(sketch, this.dom.querySelector("#container"))
        
        
    end)
    w(dom"div#container"())
end

function hue_app()
    # import a javascript library
    w = Scope(imports=["//cdnjs.cloudflare.com/ajax/libs/three.js/110/three.js"])
    # Define a Julia function with the @js macro, that will get translated to javascript
    onimport(w, @js function (THREE)
        # js"console.log(1)"
        # function sketch(three)
        #     #
        # end
        # # @new THREE.scene()
        # # printls(THREE)
        # println(js"console.log($THREE);")

        # scene = THREE.new.Scene()
        # width, height = 650, 200
        # # Create a basic perspective camera
        # camera = THREE.new.PerspectiveCamera(75, width / height, 0.1, 1000)
        # camera.position.z = 4
        # renderer = THREE.new.WebGLRenderer(Dict(:antialias => true))
        # renderer.setSize(width, height)
        # renderer.setClearColor("#fcfcfc")
        # geometry = THREE.new.BoxGeometry(1, 1, 1)
        # material = THREE.new.MeshBasicMaterial(color = "#433F81")
        # cube = THREE.new.Mesh(geometry, material);
        # scene.add(cube)
        # container = document.querySelector("#container")
        # container.appendChild(renderer.domElement);
        # slider = @manipulate for r in LinRange(0.0, 2pi, 200)
        # cube.rotation.x = r
        #     cube.rotation.y = r
        #     renderer.render(scene, camera)
        # nothing
        # end
        
        
    end)
    w(dom"div#container"())
end


function hue_app2()
  
    # w(dom"div#container"())
    # dom"button"(
    #     "Greet",
    #     events=Dict(
    #         "click" => (@js () -> alert("Hello, World!")),
    #     ),
    # )
    # myvar = [1, "foo", Dict("foo" => "bar")];
    # println(js"console.log($myvar);")
    # js"""
    # alert("Hello, World!")
    # """

    # dom"button"(
    #     "Greet",
    #     events=Dict(
    #         "click" => js"function() { alert('Hello, World!'); }",
    #     ),
    # )
    s = Scope()
    obs = Observable(s, "logme", "")
    onjs(
        s, "logme",
        js"""
        function(newValue) {
            console.log(newValue);
        }
        """
    )
end



function hue_app1()
    
    # Scope automatically asks AssetRegistry to register any local JS paths
    # you can also use a publicly available URL instead of a file path
    moment_scope = Scope(imports=["C:/Users/amira/Desktop/trial.js"])

    moment_scope.dom = node(:span, id="humantime", "<loading moment>")

    onimport(moment_scope,
        @js function (moment) # The javascript module!
            mm=moment.threejs("hiiiiiiiiiiiiiiiiiiiii")
            this.dom.querySelector("#humantime").textContent =mm.sayName()
    end)
end

function hue_app()
    
    # Scope automatically asks AssetRegistry to register any local JS paths
    # you can also use a publicly available URL instead of a file path
    moment_scope = Scope(imports=["//cdnjs.cloudflare.com/ajax/libs/three.js/110/three.js"])

    moment_scope.dom = node(:span, id="humantime", "<loading moment>")

    onimport(moment_scope,
        @js function (moment) # The javascript module!
            # mm=moment.threejs("hiiiiiiiiiiiiiiiiiiiii")
            this.dom.querySelector("#humantime").textContent =mm.Scene()
    end)
end

# Display in whatever frontend is avalaible
function main()
    if @isdefined(IJulia) || @isdefined(Juno)
        return hue_app()
    elseif @isdefined(Blink)
        win = Window()
        body!(win, hue_app())
        win
    elseif @isdefined(Mux)
        
        @sync webio_serve(page("/", req -> hue_app()), 9000)
    else
        error("do one of using Mux, using Blink before running the
               example, or run it from within IJulia or Juno")
    end
end

main()

while true;sleep(1);end