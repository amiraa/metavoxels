
// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

var gridSize=10;

rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    _rhino3dm = m; // global

    var voxelSize=json.voxelSize;

    var material=json.materials[0][1];

    setup=JSON.parse(JSON.stringify(setupEmpty));

    if(!json.rhino){
        var latticeSizeX=json.latticeSizeX;
        var latticeSizeY=json.latticeSizeY;
        var latticeSizeZ=json.latticeSizeZ;

        gridSize=json.gridSize;

        // console.log(material.area)

        const position=new THREE.Vector3(0,0,0);
        
        setup.hierarchical=json.hierarchical;
        
        setup.voxelSize=voxelSize;

        if(setup.hierarchical){
            createLattice(setup,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,createHierarchalVoxel,material);
        }else{
            createLattice(setup,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,createVoxel,material);
        }

    }else{

        var latticeSizeX=json.latticeSizeX;
        var latticeSizeY=json.latticeSizeY;
        var latticeSizeZ=json.latticeSizeZ;

        setup.hierarchical=json.hierarchical;
        
        setup.voxelSize=voxelSize;


        createGeometryFromRhino(setup,json.rhinoFileName,json.layerIndex,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,material);
    }
    

    
    var materials=[];
    var supports=[];
    var loads=[];
    var fixedDisplacements=[];
    setup.materials=json.materials;
    setup.supports=json.supports;
    setup.loads=json.loads;
    setup.fixedDisplacements=json.fixedDisplacements;

    var materials1=json.materials;
    var supports1=json.supports;
    var loads1=json.loads;
    var fixedDisplacements1=json.fixedDisplacements;

    for (var i=1;i<materials1.length;i++ ){
        var material1=materials1[i];
        var boundingMaterial=new _rhino3dm.BoundingBox(
            [
                material1[0].min.x,
                material1[0].min.y,
                material1[0].min.z
            ], 
            [
                material1[0].max.x,
                material1[0].max.y,
                material1[0].max.z
        ]);
        materials.push([ boundingMaterial,material1[1]]);
    }

    for (var i=0;i<supports1.length;i++ ){
        var support1=supports1[i];
        var boundingSupport=new _rhino3dm.BoundingBox(
            [
                support1[0].min.x,
                support1[0].min.y,
                support1[0].min.z
            ], 
            [
                support1[0].max.x,
                support1[0].max.y,
                support1[0].max.z
        ]);
        supports.push([ boundingSupport,support1[1]]);


    }
    for (var i=0;i<loads1.length;i++ ){
        var load1=loads1[i];
        var boundingLoad=new _rhino3dm.BoundingBox(
            [
                load1[0].min.x,
                load1[0].min.y,
                load1[0].min.z
            ], 
            [
                load1[0].max.x,
                load1[0].max.y,
                load1[0].max.z
        ]);
        loads.push([ boundingLoad,load1[1]]);
    }

    for (var i=0;i<fixedDisplacements1.length;i++ ){
        var fixedDisplacement1=fixedDisplacements1[i];
        var boundingFixedDisplacement=new _rhino3dm.BoundingBox(
            [
                fixedDisplacement1[0].min.x,
                fixedDisplacement1[0].min.y,
                fixedDisplacement1[0].min.z
            ], 
            [
                fixedDisplacement1[0].max.x,
                fixedDisplacement1[0].max.y,
                fixedDisplacement1[0].max.z
        ]);
        fixedDisplacements.push([ boundingFixedDisplacement,fixedDisplacement1[1]]);
    }


    changeMaterialFromBox(setup,materials);
    restrainFromBox(setup,supports);
    loadFromBox(setup,loads);
    displacementFromBox(setup,fixedDisplacements);
    
    

    // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;

    setup.viz.exaggeration=1.0;
    setup.animation.exaggeration=1.0;
    setup.viz.colorMaps=[];

    setup.numTimeSteps=json.numTimeSteps;
    setup.maxNumFiles=json.maxNumFiles;


    setup.poisson=json.poisson;
    setup.scale=json.scale;
    setup.linear=json.linear;
    setup.globalDamping=json.globalDamping;
    setup.thermal=json.thermal;
    

    saveJSON();

    console.log("Success!")
});



