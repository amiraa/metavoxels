
# Installation and Code Structure

## Installation
- Pull code from [here.](https://gitlab.cba.mit.edu/amiraa/metavoxels-code)
- Julia (I am developing using Julia 1.5.2)
  - [Download and Install Julia](https://julialang.org/downloads/)
  - [Install Julia Libraries (Packages)](https://datatofish.com/install-package-julia/)
    - Example: to a package named `PackageName` open the Julia command-line, also known as the REPL (read-eval-print-loop) and type:
      ```
      using Pkg; Pkg.add("PackageName")
      ```
    - The dependencies I am using are:
      - `IJulia`, `LinearAlgebra`, `Plots`, `JSON`, `StaticArrays`, `BenchmarkTools` and `CUDA` (optional)
    - More information to install `CUDA` can be found [here](https://juliagpu.github.io/CUDA.jl/stable/installation/overview/#InstallationOverview) 
      - The first time you use `CUDA` in julia it will take time to download the corresponding [CUDA toolkit](https://developer.nvidia.com/cuda-downloads)  
- Node.js
  - [Download and install Node.js](https://nodejs.org/en/download/)
  - in the root `metavoxels-code` folder, open the terminal/command prompt and run this command to automatically install all dependecies
      ```
      npm install
      ```
  - The dependecies I am using are:
    - `three`, `rhino3dm`, `file-system`, `edit-json-file`, `finalhandler`, `http`, `serve-static`, `child_process`

## CPU/GPU Requirements
- MetaVoxels can run on the CPU or the GPU, more info about the performance of both can be found in [this page.](./../performance/performance.md)
- For the CPU, I am using `Base.Threads` to run the code in parallel on the CPU. If you run `Threads.nthreads()`. If you want to change the number of threads, you can follow instructions in [this page](https://docs.julialang.org/en/v1/manual/multi-threading/). However, since we are using jupyter to run the code, to add threads you have to create a new kernel using the following command:
  ```Julia
  using IJulia
  IJulia.installkernel("Julia 12 Threads", env=Dict(
      "JULIA_NUM_THREADS" => "12",
  ))
  ```
- For the GPU, I am using CUDA, so make sure to have an NVIDIA GPU and CUDA toolkit installed (more details [here](https://juliagpu.gitlab.io/CUDA.jl/installation/overview/#InstallationOverview-1)). 
- A very helpful introductory on how to write gpu kernels can be found [here](https://juliagpu.gitlab.io/CUDA.jl/tutorials/introduction/#Introduction-1)
- If you don't have a local NVIDIA computer, I found it easy to use the [Satori](https://mit-satori.github.io/satori-basics.html) gpu cluster and use the V100s with jupiter notebook
  - go to this [page](https://satori-portal.mit.edu:8443/auth/realms/satori-portal-dev/protocol/openid-connect/auth?response_type=code&scope=openid&client_id=satori-portal.mit.edu&state=YKQ7UEqOcWKOdRD9QlYJcbX2BlM&redirect_uri=https%3A%2F%2Fsatori-portal.mit.edu%2Foidc&nonce=Dho2i4UdTcxRTmlAOtenGodVe-4LnUyaM2gcflFYvjU)
  - Login with your MIT kerberos
  - To transfer the files
    - On the top menu choose Clusters -> Satori Shell access and use git to clone the repo (you have to login into git first)
    - Or you can [transfer the folder](https://mit-satori.github.io/satori-getting-started.html#transferring-files) using other tools
  - On the top menu choose interactive apps -> Jupyter Notebook
  - Choose the julia kernel and navigate to the folder to open the notebook


## Tutorial
- clone: https://gitlab.cba.mit.edu/amiraa/metavoxels-code
- Jupiter Notebook Tutorial [here.](https://gitlab.cba.mit.edu/amiraa/metavoxels-code/-/blob/master/jupyter/MetaVoxel_Tutorial.ipynb)
- to run a julia jupyter kernel 
  - open julia.exe then type the following commands:
    - `using IJulia`
    - `notebook()`
  - The jupyter kernel will open and you can browse to the tutorial file



## Code Structure
- **www.gitlab.cba.mit.edu/amiraa/metavoxels-code**: current wip project for the code
  - **demos**
    - html interactive demos of simulation results
    - contains all html and styling information
  - **Julia**
    - `MetaVoxels.jl` will attach all the external and internal libraries
    - `include` folder is the most important as it contains the latest julia libraries for the cpu and gpu simulation
      - `vector.jl`
        - utils for vectors and quaternions
      - `material.jl`
        - utils for node and edge material
      - `run.jl`
        - turn json setup to cuda arrays and run simulation
      - `updateEdges.jl`
        - edges properties update
      - `updateNodes.jl`
        - nodes properties update
      - `externalForces.jl`
        - external forces applied to the system
      - `forces.jl`
        - force integration
      - `export.jl`
        - export simulation data to json
      - `plotViz.jl`
        - plotting and getting data out for analysis
      - `cuda_calls.jl`
        - CUDA related calls
  - **jupyter**
    - contains example notebooks to run the simulation and visualize the results
  - **json**
    - folder containing structures that are precomputed (either geometry or fea)
    - example of json `setup`:
      - ```json
        {
          "nodes": [
            {
              "id": "n0", 
              "parent": "11",
              "degrees_of_freedom":[0,1,2,3,4,5] , 
              "restrained_degrees_of_freedom":[true,true,true,true,true,true],
              "position": { "x": 0, "y":0,"z":0 },
              "force":{ "x": 0, "y": 10, "z":0 },
              "displacement": { "x": 0, "y": 0,"z":0 },
              "angle": { "x": 0, "y": 0,"z":0 },
              "material": {
                "cTE": 0,
                "poissonRatio": 0,
                "density": 1000,
                "area": 0.000001,
                "stiffness": 1000000
              }
            },
          ],
          
          "edges": [
              { 
                "id": "e0", 
                "source": 0, 
                "target": 1 ,
                "stress":0 ,
                "material": {
                  "cTE": 0,
                  "poissonRatio": 0,
                  "density": 1000,
                  "area": 0.000001,
                  "stiffness": 1000000
                }

             },
          ],
          "hierarchical": true,
          "voxelSize": 0.001,
          "numTimeSteps": 5000,
          "maxNumFiles": 200,
          "scale": 10000,
          
          "animation" :  {
            "showDisplacement" : true,
            "exaggeration" : 10,
            "speed":3.0
          },
          "viz" :  {
            "minStress":-500,
            "maxStress": 500,
            "colorMaps":["YlGnBu", "winter", "coolwarm","jet"],
            "colorMap":0,
          }
        }
        ``` 
      - Boundary conditions and physics properties can be also passed for the simulator in the following way
        ```json
        {
          "poisson": false,
          "linear": true,
          "globalDamping": 0.15,
          "thermal": true,
          "materials": [
            [
              {
                "max": {
                  "x": 0.01,
                  "z": 0.01,
                  "y": 0.01
                },
                "min": {
                  "x": -0.01,
                  "z": -0.01,
                  "y": -0.01
                }
              },
              {
                "cTE": 0,
                "poissonRatio": 0,
                "density": 1000,
                "area": 0.000001,
                "stiffness": 1000000
              }
            ]
          ],
          "supports": [
            [
              {
                "max": {
                  "x": 0.0005,
                  "z": 0.0015,
                  "y": 0.0015
                },
                "min": {
                  "x": -0.0005,
                  "z": -0.0005,
                  "y": -0.0005
                }
              },
              [
                true,
                true,
                true,
                true,
                true,
                true
              ]
            ]
          ],
          "loads": [
            [
              {
                "max": {
                  "x": 0.0065,
                  "z": 0.0015,
                  "y": 0.0015
                },
                "min": {
                  "x": 0.0055,
                  "z": -0.0005,
                  "y": -0.0005
                }
              },
              {
                "x": 0,
                "z": 0,
                "y": 0
              }
            ]
          ]
        }
        ````
  
      - useful if needed to create the geometry using python or any other software

  - **lib**
    - contains all external libraries used (javascript)
  - **visualization** 
    - Contains all 3D visualization and drawing code
    - `draw.js`
      - where i call all the high level commands for drawing voxels and simulation
    - `geometry.js`
      - contains low lever useful functions to create geometry (`addNode`, `addEdge`, `checkAndAdd`, `createLattice`..) 
      - I am using `rhino3dm` library in javascript to facilitate buidling intrecate geometries
    - `main.js`
      - contains all `three.js` scene building and animation
  - **node** 
    - `app.js`,`app1.js`,`serve.js`
      - contains code to run the simulation headless (either in javascript or julia) using nodejs
  - **fea**
    - old folder containing the old structure and physics simulation in javascript
    - `barFea.js`
      - FEA of truss structures (only axial forces transfered) where each element (bar element) has 6 degrees of freedom ( $F_{x1}, F_{y1}, F_{z1}, F_{x2} ,F_{y2}, F_{z2}$).
      - This is used when it is assumed that the nodes are hinges (they don't transfer moments)
    - `beamFea.js`
      - FEA of frame structures where each element (beam element) has 12 degrees of freedom ( $F_{x1}, F_{y1}, F_{z1}, M_{x1}, M_{y1}, M_{z1}$ and $F_{x2} ,F_{y2}, F_{z2}, M_{x2} ,M_{y2}, M_{z2}$).
      - To solve for the stresses and displacement the euler bernoulli equations are used. 
    - `parallelFea.js`
      - solving for the stresses and displacement using DEM method ODE
      - it's similar to mass spring but instead of the spring equation I use euler bernoulli equations were each beam has 12 degrees of freedom
      - hence I don't have to create a giant matrix to solve for stresses and displacements which make the code highly parallizable 
      - Here I am able as well to accommodate for non-linear behavior
    - `flight.js`
      - physics simulation code for drone flight simulation and control


## Units

- Nominal size (i.e. lattice dimension) (m)
<!-- - < Multiplier for how strongly gravity should affect this block in g (1.0 = -9.81m/s^2) -->
- Cached mass of this voxel (kg)
- mass moment of inertia (i.e. rotational "mass") (kg*m^2)







