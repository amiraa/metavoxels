# Drop Test

Check the [structural mechanics model](https://amiraa.pages.cba.mit.edu/metavoxels/03_Research/structureModel.html) for detailed explanation of the friction calculation (Couloumb friction model).

<img src="./3_drop.gif" width="60%" />
<img src="./3_voxel_convergence_drop.png" width="30%" />


<img src="./4_drop.gif" width="60%" />
<img src="./4_voxel_convergence_drop.png" width="30%" />

<img src="./5_drop.gif" width="60%" />
<img src="./5_voxel_convergence_drop.png" width="30%" />
