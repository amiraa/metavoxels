# Multi-Scale Hierarchical Modeling

Going from thousands of elements (FEA/DEM) to hundred of elements (Beam Theory) to tens of elements (Homogenization and hierarchy).

<img src="./main_beam.png" width="60%" />   <br>
<img src="./comp.png" width="60%" />   <br>
<img src="./hier.gif" width="60%" />   <br>

### Example:

<img src="./h.png" width="50%" /> <br>   
3292 Nodes, 11476 edges

<img src="./h1.png" width="50%" /> <br>  
830 Nodes, 2111 edges > **75% less nodes and 82% less edges**

<img src="./h2.png" width="50%" />   <br>
264 Nodes, 1034 edges > **92% less nodes and 91% less edges**

---

## Hierarchy Implementation

<img src="./homogenized _cuboct.gif" width="40%" />   <br>

- Automated workflow to extract the elasticity tensor from voxel units

- Detailed explanation [here](./../top_opt/inverse_hom/inverse_homogenization.md).
- Steps
  - Get Microstructure
  - Apply 6 main harmonic displacements
  - Solve elasticity tensor to get young's modulus and poisson ratio
    <!-- $$  -->
    ```math
    S=\frac{1}{E}\begin{vmatrix} 
    1& -\nu & -\nu & 0 & 0 &0\\\ 
    -\nu&  1 & -\nu & 0 & 0 &0\\\ 
    -\nu& -\nu &1 &  0 &0 &0\\\ 
    0&  0 &0 &2(1+\nu)&0 &0\\\ 
    0&  0 &0 &0&2(1+\nu)&  0\\\ 
    0&  0 &0 &0& 0 &2(1+\nu)\end{vmatrix}
    ```
    <!-- $$ -->
  
  - Use these properties to for coarse simulation
    - Global Properties Using Local Rules
      - [Stiffness, poisson ratio and thermal expansion](../physics/physics.md)

---

## Multi-Scale Implementation
1. Best Representation for Multi-scale nodes and edges
   1. add the tag `multiscale` to `setup`
   2. nodes
      1. add property `scale`= 1, 2, 4, 8 .. 
   3. edges
      1. add property `order` (0 to 8) to each source/target (or bake `relPos`)
         1. `0` is the nodal coordinate: ( 0, 0, 0) i.e. the center
         2. `1` is the nodal coordinate: (-1,-1,-1)
         3. `2` is the nodal coordinate: (-1, 1,-1)
         4. `3` is the nodal coordinate: (-1,-1, 1)
         5. `4` is the nodal coordinate: (-1, 1, 1)
         6. `5` is the nodal coordinate: ( 1,-1,-1)
         7. `6` is the nodal coordinate: ( 1, 1,-1)
         8. `7` is the nodal coordinate: ( 1,-1, 1)
         9. `8` is the nodal coordinate: ( 1, 1, 1)
2. How to turn forces between two nodes into moments (based on [this](https://developer.nvidia.com/gpugems/gpugems3/part-v-physics-simulation/chapter-29-real-time-rigid-body-simulation-gpus))
   1. transition of center of mass
      1. just add all forces that are applied in `updateNodes` at the beginning when updating `N_intForce` and `N_intMoment`
   2. Rotation
      1. in addition to the moment that is applied (`momentPos`/`momentNeg`), add new moment which is the cross product between the relative position and the force
3. How to get edge end position and orientation
   1. orientation is the same as parent node (`oVNeg` and `oVPos`)
   2. rotate the nodal coordinates around the center of mass of node
      1. in `updateEdges` when updating `pVNeg` and `pVPos` get position and shifted by the `relPos` oriented by `oVNeg`/`oVPos`  (is it as simple as angle normalized * `relPos`)

---
## Detailed TODO
- [x] create simple case study
  - [x] one scale 2 node surrounded by 8 scale 1 nodes
  - [x] prescribe/create json in julia/javascript
  - [x] special visualization
    - [x] for bigger scales create 8 smaller cubes that are transparent
  - [x] small force on one of the nodes
  - [x] setup add all stuff written in point 1
- [x] `run.jl`
    - [x] see what special CUDA arrays have to be added
    - [x] `scale` to be added to material node
    - [x] `multiscale` to material node
    - [x] `E_relPosSource` and `E_relPosTarget` to be added to edges
    - [x] create special struct for Edges and nodes, or just add properties that do not change like source target or relPos to edge material
- [x] check that when you angle normalized * `relPos` the position is correct
  - [x] code it (done with quaternions)
- [x] in `updateEdges`
  - [x] at the beginning update `pVNeg` and `pVPos`
  - [x] at the end add moment (cross product) to `momentPos` or `momentNeg`
- [x] in `updateNodes`
  - [x] check `N_edgeID` to add edges connected
  - [x] add all children forces 
  - [x] add all children moments
- [ ] Automatic efficient way to switch between both representations
  - [x] generation
  - [x] simulation
  - [ ] visualization


---
## Multi-Scale Hello World

<img src="./non_ax.png" width="60%" />   <br>

- non axial forces
- new hierarchial representation with nodes having parents and children
  
<img src="./multiscale.png" width="60%" />   <br>
<img src="./multiscale.gif" width="60%" />   <br>
10 nodes, 14 edges vs 3 nodes, 2 edges

---

## Multi-Scale Case Study


  
<img src="./multiscale_2.gif" width="32%" />   
<img src="./multiscale_1.gif" width="32%" />   
<img src="./multiscale_3.gif" width="32%" /> 


|               | Detailed  | Hierarchical  | Multi-scale |
| ------------- |:---------:|:-------------:| :----------:|
| # Edges       | 980       | 127           | 75          |
| # Nodes       | 320       | 62            | 41          |
| Memory        | 100%      | 14%           | 9%          |
| # Timesteps   | 50000     | 1000          | 1000        |
| Speedup       | 1x        | 124x          | 160x        |
| Accuracy      | 100%      | --            | --          |


dt ∝ particle mass, the smaller the particle the smaller dt, ∴ more timesteps needed

<br><br/>
<img src="./comp_graph.png" width="70%" /> 

----
## MADCAT

<img src="./m1.png" width="31%" /> 
<img src="./m2.png" width="31%" /> 
<img src="./m3.png" width="31%" /> 

<br><br/>

<img src="./c1.png" width="31%" /> 
<img src="./c2.png" width="31%" /> 
<img src="./c3.png" width="31%" /> 

<br><br/>


---
## Future Steps

- [x] Detailed and hierarchical model comparison 
  - [x] Choose interesting case study
    - [x] Madcat (working with Ben)
  - [ ] list physical parameters that could be changed to increase accuracy
  - [ ] graph computational speedup vs accuracies
- [ ] twist a,d anisotropic
- [ ] Timescales


