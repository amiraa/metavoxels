# Dynamic Lattice Boltzmann

## Solid to Fluid Interaction

<img src="./static.gif" width="40%" />
<img src="./staticF.gif" width="40%" /><br></br>


<img src="./dynamic.gif" width="40%" />
<img src="./dynamicF.gif" width="40%" /><br></br>

## Fluid to Solid Interaction

<img src="./lbm2D_static.gif" width="40%" />
<img src="./lbm2D_static1.gif" width="37%" /><br></br>

## Solid <---> Fluid 

<img src="./fluidSolid.gif" width="40%" />
<img src="./fluidSolidF.gif" width="40%" /><br></br>



<img src="./startBeam.png" height="300" />
<img src="./finalBeam.png" height="300" /><br></br>

<img src="./fluidSolid1.gif" width="40%" />
<img src="./fluidSolidF1.gif" width="40%" /><br></br>

<img src="./startBeam1.png" height="300" />
<img src="./finalBeam1.png" height="300" /><br></br>


## Steps

1. Set boundary conditions in and geometry in MetaVoxels
2. for *t* timeSteps
   1. Geometry conversion: solid (MetaVoxels) to LBM grid through convex hull of the nodes 
      <img src="./concave_hull.png" width="70%" />
   2. Run LBM for *n* timesteps
   3. Get pressure values at geometry edges
   4. Convert pressure to force on edge nodes (closest)
   5. Run MetaVoxels simulation for *m* timesteps

---
# 3D

<img src="./test3d.gif" height="50%" />

---
## MADCAT

<img src="./1fluidstructure3d2.gif" width="60%" />
<img src="./1fluidstructure3d.gif" width="48%" />
<img src="./1fluidstructure3d1.gif" width="48%" /> 


### Exaggerated Forces

<img src="./fluidstructure3d1.gif" width="48%" />
<img src="./fluidstructure3d.gif" width="48%" />
<img src="./fluidstructure3d2.gif" width="60%" />







   