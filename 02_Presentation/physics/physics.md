# Physics

## Structure Mechanics Model
- [Detailed Explanation](https://amiraa.pages.cba.mit.edu/metavoxels/03_Research/structureModel.html)

## Thermal Expansion and Laminate Structures

<img src="./thermal.gif" width="60%" /><br></br>
<img src="./tendon.gif" width="60%" /><br></br>

## Poisson Ratio

<img src="./ten_positive_poisson.gif" width="30%" />
<img src="./ten_zero_poisson.gif" width="30%" />
<img src="./ten_negative_poisson.gif" width="30%" /><br></br>

Compression Zero Poisson:

<img src="./comp_zero_poisson.gif" width="40%" /><br></br>
Compression Positive Poisson:

<img src="./comp_positive_poisson.gif" width="40%" /><br></br>
Compression Negative Poisson (Interesting failure mode):

<img src="./comp_negative_poisson_failure.gif" width="40%" /><br></br>

### Algorithm

```
for each node:
    calculate node strain:
        check for each direction (x,y,z)
            check attached bonds
            sum edge strain in each direction 
            check tension by two opposite links
        if there is no tension in one direction (corner node or side node)
            adjust node strain in that direction to be:
                 (((1+sum of strain in opposite directions) ^ (-poisson ratio) )- 1.0)
for each edge:
    if poisson ratio isn't 0
        incorporate node strain values in edge strain


```

## Non-Linear Deformation (using Bilinear model)

Elastic vs Plastic Deformation:

<img src="./elastic.gif" width="40%" />
<img src="./plastic.gif" width="40%" /><br></br>

## Non-Linear Deformation (using data)

<img src="./nonlinear/elasticpng.png" width="40%" />
<img src="./nonlinear/el.gif" width="55%" /><br></br>
<img src="./nonlinear/brittle.png" width="40%" />
<img src="./nonlinear/brittle.gif" width="55%" /><br></br>
<img src="./nonlinear/plastic1.png" width="40%" />
<img src="./nonlinear/plastic1.gif" width="55%" /><br></br>



