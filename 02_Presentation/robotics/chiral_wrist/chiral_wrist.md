# Chiral Wrist


## Why?

- Bill-e
  
<img src="./bille.png" width="60%" /><br/>
<img src="./bille1.png" width="40%" /><br/>


- [Voxdog](https://gitlab.cba.mit.edu/davepreiss/voxel-limb)

<img src="./dogSupported.jpg" width="40%" /><br/>

- Toyota Seat

---

## How?
- [Distributed Actuation](https://gitlab.cba.mit.edu/davepreiss/parallelized-actuators)
  
<img src="https://gitlab.cba.mit.edu/davepreiss/parallelized-actuators/-/raw/main/images/pcbMotorAnimation.gif" width="40%" /><br/>


---

## Geometry


Option 1:

<img src="./ch.png" width="60%" /><br/>


Option 2:

<img src="./prob.png" width="30%" /><br/>

<img src="../../top_opt/voxel_designer/chiral.gif" width="30%" />
<img src="../../top_opt/voxel_designer/chiral1.gif" width="30%" /><br/>

### Same fixed Displacement:
  
<img src="./comp_disp.gif" width="60%" /><br/>
<img src="./comp_disp.png" width="60%" /><br/>

### Load:
  
<img src="./comp_load.gif" width="60%" /><br/>
<img src="./comp_load.png" width="60%" /><br/>



---

### Effect of height (same distributed force)
- 1 * 1 * 1 Lattice
  
<img src="./1.png" width="60%" /><br/>

- 1 * 1 * 2 Lattice
  
<img src="./2.png" width="60%" /><br/>

- 1 * 1 * 3 Lattice
  
<img src="./3.png" width="60%" /><br/>

###  Spatial Programming Design rules 
<img src="./ben_smart_rot.png" width="60%" /><br/>
<img src="./smart_rot.png" width="60%" /><br/>

