# Rover

Case study of the design and optimization of [rover](https://gitlab.cba.mit.edu/ccameron/rover).

## Design

<img src="./dice_assembly.gif" width="75%" /><br/>

## Assembly
<img src="./assembly.gif" width="75%" /> <br/>


## Control
- [Live Demo here.](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexRover.html)
- [Check Control Function Graph](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/graph/control/control.html)

<img src="./1rover4.gif" width="40%" />
<img src="./1rover2.gif" width="40%" /><br/>
<img src="./1rover1.gif" width="40%" />
<img src="./1rover3.gif" width="40%" />



## Old Trials

<img src="./rover1.gif" height="100" />
<img src="./rover2.gif" height="100" /><br/>

<img src="./r.gif" width="30%" /><br/>

<img src="./rr.gif" width="30%" /><br/>


## Optimization
- [AI that grows](https://gitlab.cba.mit.edu/amiraa/physical-computing-design-tools/-/blob/master/02_Presentation/AI_that_grows/AI_grow.md)
- [Weight Agnostic Neural Networks](https://weightagnostic.github.io/)