# Hydrosnake 
Using the dynamic Model, I wanted to simulate the undulatory swimming motion based on [(3)](https://www-cambridge-org.libproxy.mit.edu/core/journals/journal-of-fluid-mechanics/article/optimal-undulatory-swimming-for-a-single-fishlike-body-and-for-a-pair-of-interacting-swimmers/6611B915918A10A3ED65A628A86C86C8):

![](./fish.png)
![](./fishEq.png)
<!-- $$  h(x,t)  =   h_{0}(x,t)+B(x,t)+y_{1}(x)\\ \  =   a_{0}A(x)\sin(2\pi(x/\lambda-ft+\phi))+B(x,t)+y_{1}(x)\\   =   g(x)\sin(2\pi(ft+\psi(x)))+y_{1}(x)$$ -->

I modeled the equation in this [colab notebook](https://colab.research.google.com/drive/1WtanGAAVSMysUJHzoRz3_mu7nsTrLMW7?authuser=2#scrollTo=zTkmSjy-34dq):

![](./travelingWave2D.png)
![](./travelingWave3D.png)

Demo (old):
[Live Demo Link.](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191115_NodeJsJulia/demos/indexWhale.html)


<img src="./snake.gif" width="500" /><br></br>
<!-- ![](./whale.mp4) -->

<!-- #### Friction and Gravity

![](02_Presentation/snake2.mp4)

![](02_Presentation/drop.mp4) -->