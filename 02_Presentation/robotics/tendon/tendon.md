# Modeling the Tendon Section

<img src="./tendonmorph.gif" width="75%" /><br/>




## Model 1
- [Demo](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191115_NodeJsJulia/demos/indexTendonLive.html)
  
<img src="./tendon.gif" width="75%" /><br/>

---
## Model 2


<img src="./tendon_detailed.gif" width="75%" /><br/>


Variables to tweak:
1. Rate of tension
   - tension vs time
2. rate of tension in each voxel
   - the tension in each voxel segment is different
3. order which segment is in tension first
4. 25 times slower than Model 1

### Validation Against Experimentation Results
Alfonso has the experimental test very well documented [here](https://gitlab.cba.mit.edu/alfonso/instron-opencv).

OpenCV setup:

<img src="./portada.png" width="75%" /><br/>

Results after tuning the parameters:

<img src="./val1.png" width="75%" /><br/>


---
## Model 3

Chris put together a tendon model that calculated reactions at each node in the tendon path including friction in the hopes of recreating the experimental data that Alfonso acquired. The model has just one parameter, coefficient of friction, and captures the behavior of varying deflection along the beam. 

### Initial Results

<img src="./tend.gif" width="75%" /><br/>

Damping needs more work.



---



