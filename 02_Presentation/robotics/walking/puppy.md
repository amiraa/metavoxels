# Puppy

## 4 legs
Legs movement:

<img src="./puppy/puppy_constrained.gif" width="75%" /><br/>
<img src="./puppy/puppy_constrained_opoosite.gif" width="75%" /><br/>

Constrained in Z:

<img src="./puppy/puppy_half_constrained_opoosite.gif" width="75%" /><br/>
Unconstrained:

<img src="./puppy/puppy_fallen_opoosite.gif" width="75%" /><br/>

## 6 legs

<img src="./puppy/puppy_6_fall.mp4" width="75%" /><br/>

<img src="./puppy/puppy_6_wave.mp4" width="75%" /><br/>

Making spine more stiff and decreasing simulation timestep:

<img src="./puppy/puppy_6_walking.gif" width="75%" /><br/>

4 Legged:

<img src="./puppy/puppy_stiff_falling.gif" width="75%" /><br/>

Cat Inspiration (alternating legs with a shift):

<img src="./puppy/cat.gif" width="75%" /><br/>


<img src="./puppy/puppy_half_constrained_long_alt.gif" width="75%" /><br/>

<img src="./puppy/puppy_long_alt.gif" width="75%" /><br/>
<img src="./puppy/puppy_long_alt_1.gif" width="75%" /><br/>
