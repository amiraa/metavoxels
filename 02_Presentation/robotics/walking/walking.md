# Walking Robot

Simulation and optimization of the [walker robot.](https://gitlab.cba.mit.edu/alfonso/walker/-/tree/master)



- [Demo](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexWalker.html)


Supported:

<img src="./walking.m4v" width="75%" /><br/>

First Trial:

<img src="./walking_fail2.m4v" width="75%" /><br/>

Density/Stiffness Adjusted (Slippery):

<img src="./walking_fail.m4v" width="75%" /><br/>

<img src="./walking_fail33.m4v" width="75%" /><br/>

Constrained Movement:

<img src="./walking_fail4.m4v" width="75%" /><br/>

<img src="./walking_fail44.m4v" width="75%" /><br/>

## Update 18 December

Old Symmetrical Movement:

<img src="./201218/walking2.gif" width="75%" /><br/>

Final Adjusted Movement:

<img src="./201218/final_movement.gif" width="75%" /><br/>

Constrained Walking:

<img src="./201218/final_walking.gif" width="75%" /><br/>

Unconstrained Walking:

<img src="./201218/unconstrained_walking1.gif" width="75%" /><br/>

Unconstrained Walking Weight:

<img src="./201218/unconstrained_walking_weight.gif" width="75%" /><br/>

Unconstrained Walking Weight:

<img src="./201218/unconstrained_walking_weight2.gif" width="75%" /><br/>


Unconstrained Walking Weight:

<img src="./201218/unconstrained_walking_weight_side.gif" width="75%" /><br/>


## [Puppy](./puppy.md)


