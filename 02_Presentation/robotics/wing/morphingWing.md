# Morphing Wing Simulation and Optimization

## Simulation
<img src="./sim1.gif" width="75%" /><br/>


## Optimization

- Voxel Optimization
  - [Voxel Designer](../../top_opt/voxel_designer/voxelDesignTool.md)
- Global Multi-material Optimization
  - [Topology Optimization](../../top_opt/search.md)


