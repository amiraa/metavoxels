# Concurrent Macro and Microscale Topology Optimization


Based on [1](https://link.springer.com/article/10.1007/s00158-019-02323-6) and [2](https://www.sciencedirect.com/science/article/abs/pii/S0045782518305206).

----

## Free Material Distribution Optimization 

(VTS variable thickness sheet), for multiple microstructures

```math
\begin{aligned}
& \underset{\rho}{\text{minimize}}
& & J= F^TU=U^TK^TU=\sum_{e=1}^{N_e}\rho_e U_e^TK_eU_e\\
& \text{subject to}
& & KU=F  \\
& & & \sum_{e=1}^{N_e}{\rho_e}v_e \leq V_{max} \\
& & & \rho_{min}\leq \rho_e \leq \rho_{max} \ \  \forall \ \  (e=1,2,...,N_e)\\
\end{aligned}
```

### Regularization 
Assuming $`\Theta`$ groups

```math
\bar{\rho}_{\xi}=\frac{1}{N_{\xi}} \sum _{i=1}^{N_{\xi}} \rho^i_{\xi} \ \ \ \ (\rho^{min}_{\xi}\leq \rho^i_{\xi} \le \rho^{max}_{\xi} \ \ ;\ \   \xi=1,2,...,\Theta)
```
---

## Objective Function

```math
\begin{aligned}
& {\text{find:}}
& & \rho_M^i,\rho_m^j \ \ \ (i=1,2,...N_M;\ j=1,2,...,N_m) \\
& \underset{\rho_M,\rho_m }{\text{minimize}}
& & J(\rho_M,\rho_m)=\frac{1}{2}\int_{\Omega_M} D(\rho_M,\rho_m)\varepsilon(u_M)\varepsilon(u_M) \ \ d\Omega_M  \\
& \text{subject to}
& & a(u_M,\nu_M,D_M)=l(\nu_M), \ \ \forall \nu_M \in H_{per}(\Omega_M,\mathbb{R}^d) \\
& & & a(u_m,\nu_m,D_m)=l(\nu_m), \ \ \ \forall \nu_m \in H_{per}(\Omega_m,\mathbb{R}^d) \\
& & & \sum_{e=1}^{N_M}{\rho_M}v_M \leq V_M^{max} \ \ \sum_{e=1}^{N_m}{\rho_m}v_m \leq V_m^{max} \\
& & & \phi^{min}_{M/m}\leq \phi^e_{M/m} \leq \phi^{max}_{M/m} \ \  \forall \ \  (e=1,2,...,N_{M/m})\\
\end{aligned}
```

- $`J`$ is the objective function (for example structural compliance)
- $`u_M`$ and $`u_m`$ are the macroscale and microscale displacement fields respectively
- $`\nu_M`$ and $`\nu_m`$ are the macroscale and microscale virtual displacement fields respectively
- a bilinear energy function
- $`l`$ linear load function
- Macroscale equilibrium state (based on virtual work principle):


```math

a(u_M,\nu_M,D_M)= \int_{\Omega_M} D_M(\rho_M,\rho_m)\varepsilon(u_M)\varepsilon(\nu_M) \ \ \mathbb{d}\Omega_M\\

l(\nu_M)= \int_{\Omega_M} f \nu_M \mathbb{d}\Omega_M+ \int_{\Gamma_M} h \nu_M \mathbb{d}\Gamma_M\\

a(u_m,\nu_m,D_m)= \int_{\Omega_m} D_m(\rho_m)\varepsilon(u_m)\varepsilon(\nu_m) \ \ \mathbb{d}\Omega_m\\

l(\nu_m)=\int_{\Omega_m} D_m(\rho_m)\varepsilon(u^0_m)\varepsilon(\nu_m)  \mathbb{d}\Omega_m \\

```

- $`f`$ is the body force
- $`h`$ boundary traction on the Neumann boundary $`\Gamma_M`$ of the macrostructure
- $`D_M`$ and $`D_m`$ are the stiffness tensors defined by modified SIMP approach:

```math
D_M=[c+(\rho_M)^p(1-c)]D^H \\
D_m=[c+(\rho_m)^p(1-c)]D^0
```

- $`D_0`$ is the constitutive elastic tensor of the material and $`c=1e^{-9}`$ constant to avoid singularity of the stiffness matrix
- $`D_H`$ is the homogenized stiffness tensor

## Sensitivities

```math
\frac{\delta J}{\delta \rho_M}= -\frac{1}{2}\int_{\Omega_M} p(\rho_M)^{p-1}(1-c)D^H(\rho_m)\varepsilon(u_M)\varepsilon(u_M) \ \ d\Omega_M  \\
\frac{\delta J}{\delta \rho_m}= -\frac{1}{2}\int_{\Omega_M} [c+(\rho_M)^p(1-c)] \frac{\delta D^H(\rho_m)}{\delta \rho_m}

D^H(\rho_m)\varepsilon(u_M)\varepsilon(u_M) \ \ d\Omega_M  
```

---


## Homogenization

Details [here.](./inverse_homogenization.md)


---
