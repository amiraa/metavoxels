# Elasticity (Constitutive) Tensor

Based on [1](https://jscholarship.library.jhu.edu/handle/1774.2/58621) and [2](https://www.researchgate.net/publication/261173987_Design_of_Material_Structures_Using_Topology_Optimization).

## 2D
For two dimentional orthotropic materials:

```math
C^H_{2D,orth}=
\begin{bmatrix}
C^H_{1111} & C^H_{1122} & 0\\
C^H_{1122} & C^H_{2222} & 0 \\
0   & 0   & C^H_{1212} 
\end{bmatrix}
```

For isotropic material assuming plane stress state:


```math
C^H_{2D,iso,pl,stress}= \frac{E}{1-\nu^2}
\begin{bmatrix}
1 & \nu & 0\\
\nu & 1 & 0 \\
0   & 0   & \frac{1-\nu}{2} 
\end{bmatrix}
```

## 3D

```math
C^H_{3D,orth}=
\begin{bmatrix}
C^H_{1111} & C^H_{1122} & C^H_{1133} & 0          & 0          & 0          \\
C^H_{1122} & C^H_{2222} & C^H_{2233} & 0          & 0          & 0          \\
C^H_{1133} & C^H_{2233} & C^H_{3333} & 0          & 0          & 0          \\
0          & 0          & 0          & C^H_{1212} & 0          & 0          \\
0          & 0          & 0          & 0          & C^H_{1313} & 0          \\
0          & 0          & 0          & 0          & 0          & C^H_{2323} \\
\end{bmatrix}
```
Assuming isotropy in each plane of symmetry, and that the moduli in the three principle directions are equal $`E_{1111}=E_{2222}=E_{3333}`$:


```math
C^H_{3D,orth,iso}=E 
\begin{bmatrix}
\frac{1-\nu^2_{23}}{\Delta} & \frac{\nu_{12}+\nu_{13}\nu_{23}}{\Delta}  & \frac{\nu_{13}+\nu_{12}\nu_{23}}{\Delta} & 0 & 0 & 0 \\
\frac{\nu_{12}+\nu_{13}\nu_{23}}{\Delta} & \frac{1-\nu^2_{13}}{\Delta}  & \frac{\nu_{23}+\nu_{12}\nu_{13}}{\Delta} & 0 & 0 & 0 \\
\frac{\nu_{13}+\nu_{12}\nu_{23}}{\Delta} & \frac{\nu_{23}+\nu_{12}\nu_{13}}{\Delta}  & \frac{1-\nu^2_{12}}{\Delta} & 0 & 0 & 0 \\
0 & 0  & 0  & \frac{1}{2(1+\nu_{12})}  & 0  & 0 \\
0 & 0  & 0  & 0  & \frac{1}{2(1+\nu_{13})} & 0 \\
0 & 0  & 0  & 0  & 0 & \frac{1}{2(1+\nu_{23})} \\
\end{bmatrix}
```

where:

```math
\Delta=1 -\nu^2_{12} -\nu^2_{23} -\nu^2_{13} -2\nu_{12}\nu_{23}\nu_{13} 
```

Assuming full isotropy ($`\nu_{12}=\nu_{23}=\nu_{13}=\nu`$)

```math
C^H_{3D,iso}= E \frac{1-\nu}{(1+\nu)(1-2\nu)}
\begin{bmatrix}
1          & \frac{\nu}{1-\nu}  & \frac{\nu}{1-\nu}  & 0          & 0          & 0          \\
\frac{\nu}{1-\nu} & 1           & \frac{\nu}{1-\nu}  & 0          & 0          & 0          \\
\frac{\nu}{1-\nu}  & \frac{\nu}{1-\nu}  & 1          & 0          & 0          & 0          \\
0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)}  & 0          & 0          \\
0          & 0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)} & 0          \\
0          & 0          & 0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)} \\
\end{bmatrix}
```


----

# Other Problem Formulation

```math
\begin{aligned}
& \underset{\phi}{\text{minimize}}
& & \rho=\sum^{NE}_{e=1} \kappa^e \gamma^e(\chi^e)^{1/p} \\
& \text{subject to}
& & E^*_I- \sum^{NE}_{e=1} q^e_I \chi^e =0 \ , \ I=1,...,NC\\
& & &  \chi_{min}\leq \chi^e \leq \chi_{max} \ , \ e=1,...,NE \\
& & & and: \ equilibrium \ equations \\
\end{aligned}
```

Lagrangian function can be written as:

```math
\lambda= \sum^{NE}_{e=1} \kappa^e \gamma^e(\chi^e)^{1/p}
+ \sum^{NC}_{I=1} \nu_I[E^*_I-\sum^{NE}_{e=1} q^e_I \chi^e] \\
+ \sum^{NE}_{e=1} \alpha^e(-\chi^e+\chi_{min})
+ \sum^{NE}_{e=1} \beta^e(\chi^e-\chi_{max})
```

- $`\nu_I`$ are the NC lagrangian multipliers for the NC equality constraints
- $`\alpha^e`$ and $`\beta^e`$ are lagragian multiplier for lower and upper constraints

```math
B_k^e(\nu)=\frac{\sum^{NC}_{I=1} \nu_I q_I^e}
{\frac{1}{p} \kappa^e \gamma^e(\chi^e)^{1/(p-1)}} =1
 \ , \ e=1,....,NE
```

The updating scheme:

```math
\chi_{k+1}^e=\chi_k^e(B_k^e(\nu))^{\eta}
```
- $`\eta`$ damping factor and $`k`$ iteration number

The lagrangian multipliers $`\nu_I`$ are determined iteratively:

```math
E^*_I-\sum^{NE}_{e=1} q_I^e \chi_{k+1}^e=0 \ , \ I=1,....,NC
```


----