# Inverse Homogenization

Based on [1](https://jscholarship.library.jhu.edu/handle/1774.2/58621) and [2](https://www.researchgate.net/publication/261173987_Design_of_Material_Structures_Using_Topology_Optimization).

## [Elasticity (Constitutive) Tensor](elasticity_tensor.md)

## Optimization Problem Formulation


```math
\begin{aligned}
& \underset{\phi}{\text{minimize}}
& & f(C^H(\phi,D^{(i)})) \\
& \text{subject to}
& & K (\phi)d^{(i)}=F(\phi)^{(i)} \ \ \ \forall \ i  \\
& & & g(C^H(\phi,D^{(i)})) \geq g_{min} \\
& & & \sum_{e\in \Omega}{\rho_e}v_e \leq V_{max} \\
& & & \phi_{min}\leq \phi_n \leq \phi_{max} \ \  \forall \ \  n \in \Omega \\
& & & d^{(i)} \ \  is \ \  \Omega-periodic \\
\end{aligned}
```

- $`f`$ is the objective function
- $`\phi`$ design variables
- $`K(\phi)`$ global stiffness matrix
- $`g`$ constraints like square symmetry or isotropy
- $`C^H`$ homogenized constitutive matrix
- Applying strain fields, $`d^{(i)}`$ displacements $`f^{(i)}`$ force vectors associated with the strain field

----

## Numerical Optimization

For a linear elastic homogeneous material:

- the stress tensor is symmetric ($`\alpha_{ij}=\alpha_{ji}`$)
- constitutive matrix also symmetric $`C^H_{ij}=C^H_{ji}`$
- constitutive matrix for the mechanical properties in 2D:

```math
C^H_{2D}=
\begin{bmatrix}
C^H_{11} & C^H_{12} & C^H_{13}\\
C^H_{12} & C^H_{22} & C^H_{23} \\
C^H_{13} & C^H_{23} & C^H_{33} 
\end{bmatrix}
```

- We apply test strain fields $`\varepsilon^{0(i)}`$ to the unit cell.
- Due to the symmetry it is sufficient to apply three test strain fields.
- The test fields consider normal strain state in the $`x_1`$ and $`x_2`$ directions as well as a state of pure shear.


```math
\varepsilon^{0(11)}=
\begin{bmatrix}
1 \\
0 \\
0 
\end{bmatrix} \ , \ 
\varepsilon^{0(22)}=
\begin{bmatrix}
0 \\
1 \\
0 
\end{bmatrix} \ , \ 
\varepsilon^{0(12)}=
\begin{bmatrix}
0 \\
0 \\
1 
\end{bmatrix} 
```

The element contributions to the strain energy $`q^e_{ij}`$ can be calculated from the nodal displacement vectors:

```math
q_{ij}^e= \frac{1}{|\Omega|}(d_0^{e(i)}-d^{e(i)})^TK^e(d_0^{e(j)}-d^{e(j)})
```

- $`d_0^{e(i)}`$ and $`d^{e(i)}`$ are the applied and resulting nodal displacements for the element $`e`$ corresponding to the test field $`(i)`$
- $`K^e`$ element stiffness matrix
- Then the element contribution is normalized by the size of the unit cell
- The components of the effective constitutive matrix are found as the sum of all element contributions to the strain energy withing the unit cell

```math
C^H_{ij}=\sum_{e \in \Omega}q^e_{ij}
```

---

## Mechanical Properties and Symmetry

### For square symmetry:

- $`C_{13}=C_{23}=C_{31}=C_{32}=0`$
- $`C_{11}=C_{22}`$

```math 
C^H_{2D,\ sq}=
\begin{bmatrix}
C_{11} & C_{12} & 0  \\
C_{12} & C_{11} & 0  \\
0      & 0      & C_{33} 
\end{bmatrix}
```

Error can be formulated as:

```math 
error_{sq}= (C^H_{11}-C^H_{22})^2 + (C^H_{13})^2 + (C^H_{23})^2 
```

### For isotropy:

- $`C_{13}=C_{23}=C_{31}=C_{32}=0`$
- $`C_{11}=C_{22}`$
- $`C_{33}=\frac{1}{2}(C_{11}-C_{12})`$

```math 
C^H_{2D,\ iso}=
\begin{bmatrix}
C_{11} & C_{12} & 0  \\
C_{12} & C_{11} & 0  \\
0      & 0      & \frac{1}{2}(C_{11}-C_{12}) 
\end{bmatrix}
```

```math 
error_{iso} = error_{sq}+ (C^H_{11}-(C^H_{12}+2C^H_{33}))^2+(C^H_{22}-(C^H_{12}+2C^H_{33}))^2
```

To prevent trivial solutions we normalize by the square of the objective value:

```math 
error =\frac{error_{sym}}{f^2}
```

----

## Homogenized Mechanical Properties:

- Bulk modulus- $`B^H`$
- Young's modulus- $`E^H`$
- Shear modulus- $`G^H`$
- Poisson's ratio- $`\nu^H`$

Using square symmetry, based on Hooke's law where we can describe the stresses in terms of the strains and components of the constitutive matrix:

```math
\sigma_{11}=C_{11}\varepsilon_{11} + C_{12}\varepsilon_{22}\\
\sigma_{22}=C_{12}\varepsilon_{11} + C_{22}\varepsilon_{22}\\
\tau{12}=C_{33}\varepsilon_{12}\\
```

### Bulk Modulus:

Ratio of hydrostatic stress to the relative volume change.
A state of hydrostatic stress where $`\sigma_{11}=\sigma{22}=\sigma`$ is applied:

```math
B=\frac{\sigma}{\Delta V/ V}=\frac{\sigma}{\varepsilon_{11}+\varepsilon_{22}} \\
\therefore \sigma=(C_{11}+C_{12})\varepsilon \\
\therefore B=\frac{1}{2}(C_{11}+C_{12})
```

With symmetry $`C_{11}=C{22}`$, then the objective is taken as an average over the bulk moduli in the $`x_1`$ and $`x_2`$ directions:


```math
B^H=\frac{1}{2}(\frac{1}{2}(C^H_{11}+C^H_{22})+C^H_{12}) \\
B^H_{3D}=\frac{1}{6}((C^H_{11}+C^H_{22}+C^H_{33})+C^H_{12}+C^H_{23}+C^H_{13})
```


### Youngs Modulus:

For $`x_1`$ based on Hooke's law:

```math 
E_{11}=\frac{\sigma_{11}}{\varepsilon_{11}}
```

Therefore it can be found by considering a uniaxial stress state where $`\sigma_{22}=\tau_{12}=0`$, hence:

```math
\sigma_{11}=C_{11}\varepsilon_{11}+C_{12}\varepsilon_{22} \\
0 = C_{12}\varepsilon_{11}+C_{22}\varepsilon_{22} \\
\therefore \varepsilon_{22}=- \frac{C_{12}}{C_{22}}\varepsilon_{11} \\
\therefore \sigma_{11}= C_{11}\varepsilon_{11}- \frac{(C_{12})^2}{C_{22}}\varepsilon_{11} \\
\therefore E_{11}=C_{11}-\frac{(C_{12})^2}{C_{22}} \\
```
With square symmetry, the objective will be:

```math
E^H=\frac{1}{2} (C_{11}+C_{22})-\frac{2(C_{12})^2}{C_{11}+C_{22}}
```

### Shear Modulus:

Pure shear is when $`\sigma_{11}=\sigma_{22}=0`$:

```math
\tau_{12}=G_{12}\varepsilon_{12}\\ 
\therefore G^H=C^H_{33} \\
G^H_{3D}=\frac{1}{3}(C^H_{33}+C^H_{44}+C^H_{55})
```

### Poisson Ration:

It is the negative of the ration of the transverse strain to the axial strain:

```math
\nu_{12}=-\frac{\varepsilon_{22}}{\varepsilon_{11}}
```

A uniaxial stress state is considered: $`\sigma_{22}=\tau_{12}=0`$

```math
\nu_{12}=\frac{C_{12}}{C_{11}} \\
\therefore \nu^H= \frac{2C^H_{12}}{C^H_{11}+C^H_{22}} \\
\nu^H_{3D}= \frac{C^H_{12}+C^H_{23}+C^H_{13}}{C^H_{11}+C^H_{22}+C^H_{33}}
```

----

## Penalization of Intermediate Densities:

Using SIMP:
```math
K^e(\phi)=(\rho_e^\eta+\rho_{min}^e)K^e_0
```

```math
q_{ij}^e= \frac{1}{|\Omega|}(d_0^{e(i)}-d^{e(i)})^TK^e_0(d_0^{e(j)}-d^{e(j)})
```

```math
C^H_{ij}=\sum_{e\in\Omega}(\rho_e^\eta+\rho_{min}^e)q_{ij}^e
```

---

## Sensitivities

### Mechanical Properties

Using an adjoint method the sensitivity of $`C^H_{ij}`$ is:

```math
\frac{\delta C^H_{ij}}{\delta \rho_e}=\eta \rho_e^{\eta-1}q^e_{ij}
```

```math
\frac{\delta B^H}{\delta \rho_e}= \frac{\eta \rho_e^{\eta-1}}{2}(\frac{1}{2}(q^e_{11}+q^e_{22})+q^e_{12})
```

```math
\frac{\delta E^H}{\delta \rho_e}=\eta \rho_e^{\eta-1}(\frac{q^e_{11}+q^e_{22}}{2}  - \frac{4C^H_{12}q^e_{12}}{C^H_{11}+C^H_{22}} + \frac{2(C^H_{12})^2}{(C^H_{11}+C^H_{22})^2}(q^e_{11}+q^e_{22}))
```

```math
\frac{\delta G^H}{\delta \rho_e}= \eta \rho_e^{\eta-1} q^e_{33}
```

```math
\frac{\delta \nu^H}{\delta \rho_e}=2 \eta \rho_e^{\eta-1}(\frac{q^e_{12}}{C^H_{11}+C^H_{22}}- \frac{C^H_{12}}{(C^H_{11}+C^H_{22})^2}(q^e_{11}+q^e_{22}))
```

### Mechanical Symmetries


```math 
\frac{\delta error}{\delta \rho_e} 
=\frac{\delta }{\delta \rho_e}(\frac{error_{sym}}{f^2})
=\frac{1}{f^2} \frac{\delta error_{sym}}{\delta \rho_e}- 2 \frac{error_{sym}}{f^3}\frac{\delta f}{\delta \rho_e}
```


```math 
\frac{\delta error_{sq}}{\delta \rho_e} 
= 2 \eta \rho_e^{\eta-1} ((C^H_{11}-C^H_{22})(q^e_{11}-q^e_{22}) 
+ C^H_{13} q^e_{13} + C^H_{23} q^e_{23} )
```


```math 
\frac{\delta error_{iso}}{\delta \rho_e}  
= error_{sq} \\
+ 2 \eta \rho_e^{\eta-1}(
    (C^H_{11}-(C^H_{12}+2C^H_{33})) (q^e_{11}-(q^e_{12}+2q^e_{33})) \\
+ (C^H_{22}-(C^H_{12}+2C^H_{33})) (q^e_{22}-(q^e_{12}+2q^e_{33}))
)

```
----




