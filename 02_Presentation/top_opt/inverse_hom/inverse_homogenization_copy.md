# Inverse Homogenization

Based on [1](https://jscholarship.library.jhu.edu/handle/1774.2/58621) and [2](https://www.researchgate.net/publication/261173987_Design_of_Material_Structures_Using_Topology_Optimization).

## Optimization Problem Formulation



```math
```

$$
\begin{aligned}
& \underset{\phi}{\text{minimize}}
& & f(C^H(\phi,D^{(i)})) \\
& \text{subject to}
& & K (\phi)d^{(i)}=F(\phi)^{(i)} \ \ \ \forall \ i  \\
& & & g(C^H(\phi,D^{(i)})) \geq g_{min} \\
& & & \sum_{e\in \Omega}{\rho_e}v_e \leq V_{max} \\
& & & \phi_{min}\leq \phi_n \leq \phi_{max} \ \  \forall \ \  n \in \Omega \\
& & & d^{(i)} \ \  is \ \  \Omega-periodic \\
\end{aligned}
$$

- $f$ is the objective function
- $\phi$ design variables
- $K(\phi)$ global stiffness matrix
- $g$ constraints like square symmetry or isotropy
- $C^H$ homohenized constituitive matrix
- Applying strain fields, $d^{(i)}$ displacements $f^{(i)}$ force vectors associated with the strain field

----

## Numerical Optimization

For a linear elastic homogeneous material:

- the stress tensor is symmetric ($\alpha_{ij}=\alpha_{ji}$)
- constitutive matrix also symmetric $C^H_{ij}=C^H_{ji}$
- constitutive matrix for the mechanical properties in 2D:

$$
C^H_{2D}=
\begin{bmatrix}
C^H_{11} & C^H_{12} & C^H_{13}\\
C^H_{12} & C^H_{22} & C^H_{23} \\
C^H_{13} & C^H_{23} & C^H_{33} 
\end{bmatrix}
$$

- We apply test strain fileds $\varepsilon^{0(i)}$ to the unit cell.
- Due to the symmetry it is sufficient to apply three test strain fields.
- The test fileds consider normal strain state in the $x_1$ and $x_2$ directions as well as a state of pure shear.


$$
\varepsilon^{0(11)}=
\begin{bmatrix}
1 \\
0 \\
0 
\end{bmatrix} \ , \ 
\varepsilon^{0(22)}=
\begin{bmatrix}
0 \\
1 \\
0 
\end{bmatrix} \ , \ 
\varepsilon^{0(12)}=
\begin{bmatrix}
0 \\
0 \\
1 
\end{bmatrix} 
$$

The element contributions to the strain energy $q^e_{ij}$ can be calculated from the nodal displacement vectors:

$$
q_{ij}^e= \frac{1}{|\Omega|}(d_0^{e(i)}-d^{e(i)})^TK^e(d_0^{e(j)}-d^{e(j)})
$$

- $d_0^{e(i)}$ and $d^{e(i)}$ are the aaplied and resulting nodal displacements for the element $e$ corresponding to the test field $(i)$
- $K^e$ element stiffness matrix
- Then the element contribuition is normalized by the size of the unit cell
- The components of the effective constituitive matrix are found as the sum of all element contribuitions to the strain energy withing the unit cell

$$
C^H_{ij}=\sum_{e \in \Omega}q^e_{ij}
$$

---

## Mechanical Properties and Symmetry

### For square symmetry:

- $C_{13}=C_{23}=C_{31}=C_{32}=0$
- $C_{11}=C_{22}$

$$ 
C^H_{2D,\ sq}=
\begin{bmatrix}
C_{11} & C_{12} & 0  \\
C_{12} & C_{11} & 0  \\
0      & 0      & C_{33} 
\end{bmatrix}
$$

Error can be formulated as:

$$ error_{sq}= (C^H_{11}-C^H_{22})^2 + (C^H_{13})^2 + (C^H_{23})^2 $$

### For isotropy:

- $C_{13}=C_{23}=C_{31}=C_{32}=0$
- $C_{11}=C_{22}$
- $C_{33}=\frac{1}{2}(C_{11}-C_{12})$

$$ 
C^H_{2D,\ iso}=
\begin{bmatrix}
C_{11} & C_{12} & 0  \\
C_{12} & C_{11} & 0  \\
0      & 0      & \frac{1}{2}(C_{11}-C_{12}) 
\end{bmatrix}
$$

$$ error_{iso} = error_{sq}+ (C^H_{11}-(C^H_{12}+2C^H_{33}))^2+(C^H_{22}-(C^H_{12}+2C^H_{33}))^2$$

To prevent trivial solutions we normalize by the square of the objective value:

$$ error =\frac{error_{sym}}{f^2}$$

----

## Homogenized Mechanical Properties:

- Bulk modulus- $B^H$
- Young's modulus- $E^H$
- Shear modulus- $G^H$
- Poisson's ratio- $\nu^H$

Using square symmetry, based on Hooke's law where we can descrive the stresses in terms of the strains and components of the constituitive matrix:

$$
\sigma_{11}=C_{11}\varepsilon_{11} + C_{12}\varepsilon_{22}\\
\sigma_{22}=C_{12}\varepsilon_{11} + C_{22}\varepsilon_{22}\\
\tau{12}=C_{33}\varepsilon_{12}\\
$$

### Bulk Modulus:

Ratio of hydrostatic stress to the relative volume change.
A state of hydrostatic stress where $\sigma_{11}=\sigma{22}=\sigma$ is applied:

$$
B=\frac{\sigma}{\Delta V/ V}=\frac{\sigma}{\varepsilon_{11}+\varepsilon_{22}} \\
\therefore \sigma=(C_{11}+C_{12})\varepsilon \\
\therefore B=\frac{1}{2}(C_{11}+C_{12})
$$

With symmetry $C_{11}=C{22}$, then the objective is taken as an average over the bulk moduli in the $x_1$ and $x_2$ directions:

$$
B^H=\frac{1}{2}(\frac{1}{2}(C^H_{11}+C^H_{22})+C^H_{12})
$$

### Youngs Modulus:

For $x_1$ based on Hooke's law:

$$ 
E_{11}=\frac{\sigma_{11}}{\varepsilon_{11}}
$$

Therfore it can be found by considering a uniaxial stress state where $\sigma_{22}=\tau_{12}=0$, hence:

$$
\sigma_{11}=C_{11}\varepsilon_{11}+C_{12}\varepsilon_{22} \\
0 = C_{12}\varepsilon_{11}+C_{22}\varepsilon_{22} \\
\therefore \varepsilon_{22}=- \frac{C_{12}}{C_{22}}\varepsilon_{11} \\
\therefore \sigma_{11}= C_{11}\varepsilon_{11}- \frac{(C_{12})^2}{C_{22}}\varepsilon_{11} \\
\therefore E_{11}=C_{11}-\frac{(C_{12})^2}{C_{22}} \\
$$
With square symmetry, the objective will be:

$$
E^H=\frac{1}{2} (C_{11}+C_{22})-\frac{2(C_{12})^2}{C_{11}+C_{22}}
$$

### Shear Modulus:

Pure shear is when $\sigma_{11}=\sigma_{22}=0$:

$$
\tau_{12}=G_{12}\varepsilon_{12}\\ 
\therefore G^H=C^H_{33}
$$

### Poisson Ration:

It is the negative of the ration of the transverse strain to the axial strain:

$$
\nu_{12}=-\frac{\varepsilon_{22}}{\varepsilon_{11}}
$$

A uniaxial stress state is considered: $\sigma_{22}=\tau_{12}=0$

$$
\nu_{12}=\frac{C_{12}}{C_{11}} \\
\therefore \nu^H= \frac{2C^H_{12}}{C^H_{11}+C^H_{22}}
$$

----

## Penalization of Intermediate Densities:

Using SIMP:
$$
K^e(\phi)=(\rho_e^\eta+\rho_{min}^e)K^e_0
$$


$$
q_{ij}^e= \frac{1}{|\Omega|}(d_0^{e(i)}-d^{e(i)})^TK^e_0(d_0^{e(j)}-d^{e(j)})
$$

$$
C^H_{ij}=\sum_{e\in\Omega}(\rho_e^\eta+\rho_{min}^e)q_{ij}^e
$$

---

## Sensitivities

### Mechanical Properties

Using an adjoint method the sensitivity of $C^H_{ij}$ is:

$$
\frac{\delta C^H_{ij}}{\delta \rho_e}=\eta \rho_e^{\eta-1}q^e_{ij}
$$

$$
\frac{\delta B^H}{\delta \rho_e}= \frac{\eta \rho_e^{\eta-1}}{2}(\frac{1}{2}(q^e_{11}+q^e_{22})+q^e_{12})
$$

$$
\frac{\delta E^H}{\delta \rho_e}=\eta \rho_e^{\eta-1}(\frac{q^e_{11}+q^e_{22}}{2}  - \frac{4C^H_{12}q^e_{12}}{C^H_{11}+C^H_{22}} + \frac{2(C^H_{12})^2}{(C^H_{11}+C^H_{22})^2}(q^e_{11}+q^e_{22}))
$$

$$
\frac{\delta G^H}{\delta \rho_e}= \eta \rho_e^{\eta-1} q^e_{33}
$$

$$
\frac{\delta \nu^H}{\delta \rho_e}=2 \eta \rho_e^{\eta-1}(\frac{q^e_{12}}{C^H_{11}+C^H_{22}}- \frac{C^H_{12}}{(C^H_{11}+C^H_{22})^2}(q^e_{11}+q^e_{22}))
$$

### Mechanical Symmetries


$$ \frac{\delta error}{\delta \rho_e} 
=\frac{\delta }{\delta \rho_e}(\frac{error_{sym}}{f^2})
=\frac{1}{f^2} \frac{\delta error_{sym}}{\delta \rho_e}- 2 \frac{error_{sym}}{f^3}\frac{\delta f}{\delta \rho_e}
$$


$$ \frac{\delta error_{sq}}{\delta \rho_e} 
= 2 \eta \rho_e^{\eta-1} ((C^H_{11}-C^H_{22})(q^e_{11}-q^e_{22}) 
+ C^H_{13} q^e_{13} + C^H_{23} q^e_{23} )
$$


$$ \frac{\delta error_{iso}}{\delta \rho_e}  
= error_{sq} \\
+ 2 \eta \rho_e^{\eta-1}(
    (C^H_{11}-(C^H_{12}+2C^H_{33})) (q^e_{11}-(q^e_{12}+2q^e_{33})) \\
+ (C^H_{22}-(C^H_{12}+2C^H_{33})) (q^e_{22}-(q^e_{12}+2q^e_{33}))
)

$$
----

# Other Problem Formulation


$$
\begin{aligned}
& \underset{\phi}{\text{minimize}}
& & \rho=\sum^{NE}_{e=1} \kappa^e \gamma^e(\chi^e)^{1/p} \\
& \text{subject to}
& & E^*_I- \sum^{NE}_{e=1} q^e_I \chi^e =0 \ , \ I=1,...,NC\\
& & &  \chi_{min}\leq \chi^e \leq \chi_{max} \ , \ e=1,...,NE \\
& & & and: \ equilibrium \ equations \\
\end{aligned}
$$

Lagrangian fuction can be writtern as:

$$
\lambda= \sum^{NE}_{e=1} \kappa^e \gamma^e(\chi^e)^{1/p}
+ \sum^{NC}_{I=1} \nu_I[E^*_I-\sum^{NE}_{e=1} q^e_I \chi^e] \\
+ \sum^{NE}_{e=1} \alpha^e(-\chi^e+\chi_{min})
+ \sum^{NE}_{e=1} \beta^e(\chi^e-\chi_{max})
$$

- $\nu_I$ are the NC lagragian multipliers for the NC equality constraints
- $\alpha^e$ and $\beta^e$ are lagragian multiplier for lower and upper constraints

$$
B_k^e(\nu)=\frac{\sum^{NC}_{I=1} \nu_I q_I^e}
{\frac{1}{p} \kappa^e \gamma^e(\chi^e)^{1/(p-1)}} =1
 \ , \ e=1,....,NE
$$

The updating scheme:

$$
\chi_{k+1}^e=\chi_k^e(B_k^e(\nu))^{\eta}
$$
- $\eta$ damping factor and $k$ iteration number

The lagrangian multipliers $\nu_I$ are determined iteratively:

$$
E^*_I-\sum^{NE}_{e=1} q_I^e \chi_{k+1}^e=0 \ , \ I=1,....,NC
$$


$$

$$

----

# Elasticity (Constitutive) Tensor

## 2D
For two dimentional orthotropic materials:

$$
C^H_{2D,orth}=
\begin{bmatrix}
C^H_{1111} & C^H_{1122} & 0\\
C^H_{1122} & C^H_{2222} & 0 \\
0   & 0   & C^H_{1212} 
\end{bmatrix}
$$
For isotropic material assuming plane stress state:


$$
C^H_{2D,iso,pl,stress}= \frac{E}{1-\nu^2}
\begin{bmatrix}
1 & \nu & 0\\
\nu & 1 & 0 \\
0   & 0   & \frac{1-\nu}{2} 
\end{bmatrix}
$$

## 3D

$$
C^H_{3D,orth}=
\begin{bmatrix}
C^H_{1111} & C^H_{1122} & C^H_{1133} & 0          & 0          & 0          \\
C^H_{1122} & C^H_{2222} & C^H_{2233} & 0          & 0          & 0          \\
C^H_{1133} & C^H_{2233} & C^H_{3333} & 0          & 0          & 0          \\
0          & 0          & 0          & C^H_{1212} & 0          & 0          \\
0          & 0          & 0          & 0          & C^H_{1313} & 0          \\
0          & 0          & 0          & 0          & 0          & C^H_{2323} \\
\end{bmatrix}
$$
Assuming isotropy in each plane of symmetry, and that the moduli in the three principle directions are equal $E_{1111}=E_{2222}=E_{3333}$:


$$
C^H_{3D,orth,iso}=E 
\begin{bmatrix}
\frac{1-\nu^2_{23}}{\Delta} & \frac{\nu_{12}+\nu_{13}\nu_{23}}{\Delta}  & \frac{\nu_{13}+\nu_{12}\nu_{23}}{\Delta} & 0                        & 0              & 0          \\
\frac{\nu_{12}+\nu_{13}\nu_{23}}{\Delta} & \frac{1-\nu^2_{13}}{\Delta}  & \frac{\nu_{23}+\nu_{12}\nu_{13}}{\Delta} & 0                        & 0              & 0          \\
\frac{\nu_{13}+\nu_{12}\nu_{23}}{\Delta} & \frac{\nu_{23}+\nu_{12}\nu_{13}}{\Delta}  & \frac{1-\nu^2_{12}}{\Delta} & 0                        & 0              & 0          \\
0                           & 0                            & 0                           & \frac{1}{2(1+\nu_{12})}  & 0              & 0          \\
0                           & 0                            & 0                           & 0                        & \frac{1}{2(1+\nu_{13})} & 0          \\
0                           & 0                            & 0                           & 0                        & 0               & \frac{1}{2(1+\nu_{23})} \\
\end{bmatrix}
$$

where:

$$
\Delta=1 -\nu^2_{12} -\nu^2_{23} -\nu^2_{13} -2\nu_{12}\nu_{23}\nu_{13} 
$$

Assuming full isotropy ($\nu_{12}=\nu_{23}=\nu_{13}=\nu$)

$$
C^H_{3D,iso}= E \frac{1-\nu}{(1+\nu)(1-2\nu)}
\begin{bmatrix}
1          & \frac{\nu}{1-\nu}  & \frac{\nu}{1-\nu}  & 0          & 0          & 0          \\
\frac{\nu}{1-\nu} & 1           & \frac{\nu}{1-\nu}  & 0          & 0          & 0          \\
\frac{\nu}{1-\nu}  & \frac{\nu}{1-\nu}  & 1          & 0          & 0          & 0          \\
0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)}  & 0          & 0          \\
0          & 0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)} & 0          \\
0          & 0          & 0          & 0          & 0          & \frac{1-2\nu}{2(1-\nu)} \\
\end{bmatrix}
$$


----
