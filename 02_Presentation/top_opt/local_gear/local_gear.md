# Local Online Search (Gears)


<img src="./square.gif" width="75%" /> <br></br>

General Algorithm:

```
If alive
    If force > threshold or derailed from neighbors
        If not inner node (neighbors<6)
            Kill
Else if dead
    Increase counter of time-step dead
    If dead after Td time-steps 
        If at least K neighbor alive
            wake up		
```

How to re-grow?

```
Get list of alive neighbors
    Get their positions
        calculate average their acceleration, velocity and rotation angle
            Infer particle position from rotation angle
            set acceleration and velocity as the calculated averagae
```




## Hyper parameters

- Force threshold
  - absolute threshold
    - set absolute force threshold
    - cons: right gear gets eroded first
      - Asymmetric
        - <img src="./4.png" width="75%" /> <br></br>
  - relative threshold 
    - look at neighbors and if bigger than threshold
    - <img src="./3.png" width="75%" /> <br></br>
- cleaning up artifacts (don't kill <5 neighbors)
  - with
    - <img src="./6.png" width="75%" /> <br></br>
  - without
    - <img src="./5.png" width="75%" /> <br></br>
- Time to regrow back
  - never
  - 1000 time-steps
  - 20000 time-steps
- K neighbors alive when regrow
  - left min 4 neighbors, right min 3 neighbor alive
  - <img src="./1.png" width="75%" /> <br></br>
