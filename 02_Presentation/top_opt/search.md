# Search
# Topology Optimization 

<!-- ..using local density constraints instead of the global volume fraction constraint[Wu et al.](https://arxiv.org/pdf/1308.04366.pdf).  -->
Implementation of gradient and non gradient based topology optimization in Julia to compile a rich library with interchangeable objective functions and design representations (density, truss/beam, frep(level set)) for different physics (structural mechanics, heat transfer and fluids lattice boltzmann).

Main library/code in [here.](https://gitlab.cba.mit.edu/amiraa/metavoxels-code/-/tree/master/voxel_designer/julia/include)

---

## [1. Minimum & Desired Compliance  Compliance](./top_opt/reg.md)

<img src="./top3d.gif" width="40%" />
<img src="./top3dcomsim.gif" width="40%" /><br></br>


## [2. Multi-material](./top_opt/multimaterial.md)

<img src="./multtop_3d.gif" width="40%" />
<img src="./multtop_3d_compliant.gif" width="40%" /><br></br>

## [3. Microstructure Design](./top_opt/microstructure.md)
<img src="./microstructPoisson1.gif" width="37%" />
<img src="./micr_3d/poisson_0.5_3_1.5_1.gif" width="40%" /><br></br>


## [4. Concurrent Topology Optimization](./top_opt/concurrent.md)


<img src="./micr_3d/macroU_1.gif" width="40%" />
<img src="./micr_3d/microU_1.gif" width="40%" /><br></br>

## [5. Space-Time Topology Optimization](./space_time/space_time.md)


<img src="./space_time/animXphys_0.gif" width="40%" />
<img src="./space_time/animtPhys_0.gif" width="40%" /><br></br>

## [6. Hybrid Cellular Automata (Online non-gradient based Optimization)](./top_opt/online.md)

<img src="./search2.gif" width="40%" /><br></br>

----

