# Space Time Topology Optimization

In space-time topology optimization, structural properties may be evaluated not only for the final (complete) structure, but also for the incomplete structure, i.e., at intermediate stages of the manufacturing process. Based on this paper ["Space-time topology optimization for additive manufacturing"](https://link.springer.com/article/10.1007/s00158-019-02420-6) from TU Delft.


## Objective Function
```math
\quad J(\boldsymbol{\rho}, \boldsymbol{t}) = J_{complete}(\boldsymbol{\rho},\boldsymbol{t}) + \theta * J_{process}(\boldsymbol{\rho}, \boldsymbol{t}),
```

## Problem Formulation

```math
\begin{aligned}
&\underset{{\boldsymbol{\phi}, \boldsymbol{\tau}}}{\min} 
& & c=\boldsymbol{U}^{\mathsf{T}}\boldsymbol{K}(\boldsymbol{\rho}) \\
& \text{subject to}
& & \boldsymbol{K}(\boldsymbol{\rho})\boldsymbol{U} = \boldsymbol{F} \\
& & &  \sum\limits_{e} \rho_{e} v_{e} \leq V_{0} \\
& & &  0 \le \phi_{e} \le 1, \\
& & & 0 \le \tau_{e} \le 1, \\
& & & V^{[{T}_{i}]}= \sum_{e} \rho^{[{T}_{i}]}_{e} v_{e} \le \frac{i}{N} V_{0}& & & & &  i=1,2,...,N ,\\
& & & \frac{1}{\#(\mathcal{M})} \sum\limits_{e\in \mathcal{M}} H(g(t_{e})) < \epsilon,\\

\end{aligned}
```

- $N$ is the number of stages
- Intermediate structures:
    ```math
    \rho^{[{T}]}_{e} = \left\{\begin{array}{llll} \rho_{e}, & \text{if } t_{e} \le {T}, \\ 0,  & \text{otherwise.} \end{array}\right.
    ```
- Continuity constraints on intermediate structures (preventing isolated material patches):
    ```math
    g(t_{e}) = \min\limits_{i \in \mathcal{N}_{e}} (t_{i}) - t_{e} \le 0, \ \ \  \forall e\in \mathcal{M},
    ```
    - $`\mathcal{N}_{e}`$ et of elements adjacent to element $`e`$.
    - $`\mathcal{M}`$ is the set of active elements in the design domain.
    - Since the above function is non-differentiable, a relaxation can be made:
        ```math
        \min\limits_{i \in \mathcal{N}_{e}} (t_{i}) = 1 - \max\limits_{i \in \mathcal{N}_{e}} (1 - t_{i}),\\
        \max\limits_{i \in \mathcal{N}_{e}} (1 - t_{i}) \approx (\sum\limits_{i \in \mathcal{N}_{e}}(1-t_{i})^{p})^{\frac{1}{p}}\\
        g(t_{e}) \approx 1 - (\sum\limits_{i \in \mathcal{N}_{e}}(1-t_{i})^{p})^{\frac{1}{p}} - t_{e} \\
        \max\limits_{e\in \mathcal{M}} (g(t_{e})) \le 0, \\
        {\mathcal{H}(\boldsymbol{t})=}\frac{1}{\#(\mathcal{M})} \sum\limits_{e\in \mathcal{M}} {H}(g(t_{e})) < \epsilon \\
        {H}(x)=\left\{ \begin{array}{llll} 1, & & x > 0, \\ 0, & & x \le 0 . \end{array} \right.  \approx \frac{1}{2} \left( \tanh{(\beta_{m} x)} + 1\right),

        ```

---

# Bridge Design

<img src="./bridgeProblem.png" width="80%" /><br></br>

<!-- 
<img src="./animXphys_0.0.gif" width="50%" /><br></br>
<img src="./animtPhys_0.0.gif" width="50%" /><br></br>
<img src="./theta_0.png" width="50%" /><br></br> -->

## $`\theta`$=0 

Final:

<img src="./theta_0.0.png" width="50%" /><br></br>

<img src="./animXphys_0.0.gif" width="50%" /><br></br>
<img src="./animtPhys_0.0.gif" width="50%" /><br></br>

## $`\theta`$=0.5

Final:

<img src="./theta_0.5.png" width="50%" /><br></br>


<img src="./animXphys_0.5.gif" width="50%" /><br></br>
<img src="./animtPhys_0.5.gif" width="50%" /><br></br>

