# 4. Concurrent Topology Optimization:

Detailed Explanation [here.](../inverse_hom/concurrent.md)
## a. One microstructure
### 2D

Macroscale & Microscale

<img src="../micr_3d/macro_1.gif" width="40%" />
<img src="../micr_3d/micro_1.gif" width="40%" /><br></br>

<img src="../micr_3d/macro_2.gif" width="40%" />
<img src="../micr_3d/micro_2.gif" width="40%" /><br></br>

### 3D
Macroscale & Microscale

<img src="../micr_3d/macro_4.png" width="40%" />
<img src="../micr_3d/micro_4.png" width="40%" /><br></br>

<img src="../micr_3d/macro_3.png" width="40%" />
<img src="../micr_3d/micro_3.png" width="40%" /><br></br>

----

## b. Multiple microstructures

### 2D
Choosing the number of microstructures and their locations:

<img src="../micr_3d/free.png" width="40%" />
<img src="../micr_3d/free2.png" width="40%" /><br></br>

<img src="../micr_3d/free1.png" width="40%" />
<img src="../micr_3d/free21.png" width="40%" /><br></br>


Final results:

<img src="../micr_3d/multi_macro_2d.png" width="40%" />
<img src="../micr_3d/multi_macro_2d2.png" width="40%" /><br></br>

<img src="../micr_3d/multi_micro_2d.png" width="40%" />
<img src="../micr_3d/multi_micro_2d2.png" width="40%" /><br></br>


### 3D

<img src="../micr_3d/free3.png" width="40%" />
<img src="../micr_3d/free31.png" width="40%" /><br></br>

Final results:

<img src="../micr_3d/FinalMacro_xPhys3.png" width="60%" /><br></br>

<img src="../micr_3d/FinalMicro_xPhys3_2_0.3.png" width="20%" />
<img src="../micr_3d/FinalMicro_xPhys3_3_0.5.png" width="20%" />
<img src="../micr_3d/FinalMicro_xPhys3_4_0.7.png" width="20%" />
<img src="../micr_3d/FinalMicro_xPhys3_5_1.png" width="20%" /><br></br>

<img src="../micr_3d/FinalMicro_Array_xPhys3_2.png" width="20%" />
<img src="../micr_3d/FinalMicro_Array_xPhys3_3.png" width="20%" />
<img src="../micr_3d/FinalMicro_Array_xPhys3_4.png" width="20%" />
<img src="../micr_3d/FinalMicro_Array_xPhys3_5.png" width="20%" /><br></br>

---
## c. Multiple microstructures with Kmeans clustering

### 2D Minimum Compliance

<img src="../micr_3d/free2.png" width="40%" />
<img src="../micr_3d/elbow.png" width="35%" /><br></br>


<img src="../micr_3d/clust.png" width="40%" />
<img src="../micr_3d/hclust.png" width="40%" /><br></br>

Final results:

<img src="../micr_3d/macroU_1.gif" width="40%" />
<img src="../micr_3d/microU_1.gif" width="40%" /><br></br>

<!-- <img src="../micr_3d/finalArrayMicro1.png" width="15%" />
<img src="../micr_3d/finalArrayMicro2.png" width="15%" />
<img src="../micr_3d/finalArrayMicro3.png" width="15%" />
<img src="../micr_3d/finalArrayMicro4.png" width="15%" />
<img src="../micr_3d/finalArrayMicro5.png" width="15%" />
<img src="../micr_3d/finalArrayMicro6.png" width="15%" /><br></br> -->

|      |![](../micr_3d/finalArrayMicro1.png) |![](../micr_3d/finalArrayMicro2.png)|![](../micr_3d/finalArrayMicro3.png)|![](../micr_3d/finalArrayMicro4.png)|![](../micr_3d/finalArrayMicro5.png)|![](../micr_3d/finalArrayMicro6.png)|
| :----:  | :----: |  :---: |:---: |:---: |:---: |:---: |
| Ex      | 0.32 | 0.27  | 0.187   |0.321   |0.298   |0.15   |
| Ey   | 0.102 | 0.103  | 0.255   |0.093   |0.088  |0.098   |
| ux   | 0.506  | 0.439   | 0.182   |0.307   |0.488   |0.646   |
| uy   | 0.161 | 0.171   | 0.249   |0.088  |0.144   |0.417   |
| S    | 0.055 | 0.058   | 0.047   |0.036   |0.049   |0.071   |


### 2D Desired Compliance

<img src="../micr_3d/free4.png" width="40%" /><br></br>


<img src="../micr_3d/free41.png" width="40%" /><br></br>

Final results:

<img src="../micr_3d/inv.png" width="40%" /><br></br>


<img src="../micr_3d/macroUComp1.gif" width="40%" />
<img src="../micr_3d/microUComp1.gif" width="40%" /><br></br>


|      | ![](../micr_3d/finalArrayMicro1c.png) | ![](../micr_3d/finalArrayMicro2c.png)| ![](../micr_3d/finalArrayMicro3c.png)| ![](../micr_3d/finalArrayMicro4c.png)|
| :----:  | :----: |  :---: |:---: |:---: |
| Ex      | 0.151 | 0.168  | 0.229   |0.316   |
| Ey   | 0.197 | 0.101  | 0.13   |0.113   |
| ux   | 0.259  | 0.584   | 0.497   |0.277   |
| uy   | 0.338 | 0.349   | 0.282   |0.099  |
| S    | 0.047 | 0.047   | 0.061   |0.035   |


### 3D Minimum Compliance

<img src="../micr_3d/3d_U/mbb/fmm.png" width="40%" /><br></br>

<img src="../micr_3d/3d_U/mbb/clust.png" width="40%"> 
<img src="../micr_3d/3d_U/mbb/hclust.png" width="40%" /><br></br>

Final results:

<img src="../micr_3d/3d_U/mbb/Macro_xPhys_1_0.3_3_2_100.png" width="40%" /><br></br>

<img src="../micr_3d/3d_U/mbb/Micro_xPhys3U_1_1_0.5_2_100.png" width="18%" />
<img src="../micr_3d/3d_U/mbb/Micro_xPhys3U_1_2_0.5_2_100.png" width="18%" />
<img src="../micr_3d/3d_U/mbb/Micro_xPhys3U_1_3_0.5_2_100.png" width="18%" />
<img src="../micr_3d/3d_U/mbb/Micro_xPhys3U_1_4_0.5_2_100.png" width="18%" />
<img src="../micr_3d/3d_U/mbb/Micro_xPhys3U_1_5_0.5_2_100.png" width="18%" /><br></br>

<img src="../micr_3d/3d_U/mbb/final3dMBB.png" width="40%" />
<img src="../micr_3d/3d_U/mbb/final3dsymMBB.png" width="40%" /><br></br>

### 3D Desired Compliance
<img src="../micr_3d/3d_U/inv/fmm.png" width="40%" /><br></br>

<img src="../micr_3d/3d_U/inv/clust.png" width="40%"> 
<img src="../micr_3d/3d_U/inv/hclust.png" width="40%" /><br></br>

Final results:

<img src="../micr_3d/3d_U/inv/final3dInver.png" width="40%" />
<img src="../micr_3d/3d_U/inv/final3dsymInver.png" width="40%" /><br></br>

<img src="../micr_3d/3d_U/inv/C1Micro_xPhys3U_1_0.5_2_150.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C1Micro_xPhys3U_2_0.5_2_150.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C1Micro_xPhys3U_3_0.5_2_150.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C1Micro_xPhys3U_4_0.5_2_150.png" width="22%" /><br></br>

<img src="../micr_3d/3d_U/inv/C20Micro_xPhys3UArray_1.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C20Micro_xPhys3UArray_2.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C20Micro_xPhys3UArray_3.png" width="22%" />
<img src="../micr_3d/3d_U/inv/C20Micro_xPhys3UArray_4.png" width="22%" /><br></br>



-----