# 3. Microstructure Design:

Detailed Explanation [here.](../inverse_hom/inverse_homogenization.md)
## 2D
### Maximum Bulk Modulus
  
<img src="../microstruct.gif" width="30%" /> 
<img src="../microstructBulk.gif" width="30%" /><br></br>

### Maximum Shear Modulus
  
<img src="../microstructShear.gif" width="30%" /><br></br>

### Negative Poisson Ratio
  
<img src="../microstructPoisson.gif" width="30%" />
<img src="../microstructPoisson1.gif" width="30%" /><br></br>

## 3D
### Maximum Bulk Modulus
  
<img src="../micr_3d/bulk_0.2_5_1.5_2_MMA.gif" width="40%" />
<img src="../micr_3d/bulk3DD_0.2_5_1.5_2.gif" width="40%" /><br></br>

### Maximum Shear Modulus
  
<img src="../micr_3d/shear_0.2_5_5_2.gif" width="40%" />
<img src="../micr_3d/shear_0.5_5_5_1.gif" width="40%" /><br></br>

### Negative Poisson Ratio
  
<img src="../micr_3d/poisson_0.2_5_1.5_2.gif" width="40%" /><br></br>
<img src="../micr_3d/poisson_0.2_5_1_1.gif" width="40%" />
<img src="../micr_3d/poisson_0.5_3_1.5_1.gif" width="40%" /><br></br>

-----

## Fabrication Constraints
Unconstrained vs constrained results (same optimization parameters)

<img src="../micr_3d/fab/cond0.png" width="40%" />
<img src="../micr_3d/fab/cond.png" width="40%" /><br></br>

### Maximum Shear Modulus

<img src="../micr_3d/fab/shear_0.2_5_1.5_2_50.png" width="42.5%" />
<img src="../micr_3d/fab/shear_0.2_5_1.5_2_50_1.png" width="40%" /><br></br>


### Negative Poisson Ratio
<img src="../micr_3d/fab/poisson_0.5_3_1.5_1_50_0.png" width="40%" />
<img src="../micr_3d/fab/poisson_0.2_5_2_2_50_1.png" width="40%" /><br></br>

### Maximum Youngs Modulus

<img src="../micr_3d/fab/young_0.4_5_2_2_50_0_4.png" width="40%" />
<img src="../micr_3d/fab/young_0.5_5_2_2_34_1_4.png" width="40%" /><br></br>


-----