## 3. Multi Material:

### 2D

Red E=0.9, Green E=3, Blue E=1, Black(void) E=1e-9

<img src="../multi.gif" width="40%" /><br></br>

### 3D

<img src="../multtop_3d.gif" width="30%" />
<img src="../multtop_3d.png" width="40%" /><br></br>

<img src="../multtop_3d_compliant.gif" width="30%" />
<img src="../multtop_3d_compliant.png" width="40%" /><br></br>

-----