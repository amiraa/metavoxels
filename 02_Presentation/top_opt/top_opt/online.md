<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0MathJax.js?config=TeX-AMS_CHTML"></script>

# Local Opimization: Hybrid Cellular Automata

![](./../hca/hca_strut.gif)

## Background and Approach:

### [1- A Cellular Automaton Generating Topological Structures (1994)](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/2361/0000/Cellular-automaton-generating-topological-structures/10.1117/12.184866.short?SSO=1)

<img src="../hca/connection.png" width="300"  /><br></br>

#### Algorithm:
1. Define cells, load, fixed conditions
2. calculate local stresses in cells
3. change material properties of cells based on update rule:
```math 
E^{t+1}=E^t(1+ \alpha ( \sigma / \sigma_c -1)
```
5. Chang cell state ("death", "birth" or "division")
6. return to step 2
   
<img src="../hca/res_1.png" width="300"  /><br></br>


### [2- Topology Optimization Using a Hybrid Cellular Automaton Method With Local Control Rules](https://asmedigitalcollection.asme.org/mechanicaldesign/article-abstract/128/6/1205/477095/Topology-Optimization-Using-a-Hybrid-Cellular?redirectedFrom=fulltext)



**State** $`\alpha_i`$ of each cell is defined by **design variables** $`x_i(t)`$ (density,geometry,elastic modulus), and **field variables** $`S_i(t)`$ (stress, stain, strain energy density) (the relation between the amount of energy employed to deform a volume unit of a solid and imposed strain)).
```math 
\alpha_i = \{x_i{(t)}, S_i(t) \}
```

Strain Energy defined as follows:
```math 
U= \sum_{i=1}^{N}U_i,v_i
```
$`U_i`$ Strain energy density, $`v_i`$ volume

The local minimization problem defined as:
```math 
\min_{x_i} |e_i| \\ s.t. \  0<x_i^{min} \leq x_i \leq 1 
```

The error signal  $`e_i`$:
```math 
e_i= \bar{U}_i-U^*_i
```


$`U^*_i`$ is the local SED target,$`\bar{U}_i`$ is the average SED value:

```math 
\bar{U}_i=\frac{U_1+ \sum_{j=1}^{N}U_j }{N+1} 
```

*Neighborhood*:

![](./img/hca/neigh.png)

*Local Control Rules*:
1. Two position control
   ```math 
   \Delta x_i(t)=c_T * sgn[e_i(t)] 
   ```
2. Proportional control
   ```math 
   \Delta x_i(t)=c_p * e_i(t) 
   ```
3. Derivative control
   ```math 
   \Delta x_i(t)=c_d * (e_i(t)-e_i(t-1))
   ```
4. Integral control
   ```math 
   \Delta x_i(t)=c_I * \sum_{\tau=0}^{t} (e_i(t-\tau)) 
   ```
   


#### Algorithm:

1. Define design domain, load cases, fixed conditions
2. Define field variables (Simulation)
3. calculate error signals
4. Apply local rules and update the design variables
5. check for convergence if not return to step 2

<img src="../hca/algo.png" width="300"  /><br></br>

----
## Progress

### Static Shape Optimization

### Dynamic Optimization:

<img src="../search.gif" width="60%" /><br></br>
<img src="../search2.gif" width="60%" /><br></br>

[Jupyter Notebook](https://amiraa.pages.cba.mit.edu/cpp_frep/HCA_notebook.html)

<img src="../hca/load_1.gif" width="600"  /><br></br>
<img src="../hca/hca_strut.gif" width="600"  /><br></br>



### Dynamic Shape Optimization
Target minimization problem defined as:
```math 
\min_{x_i} |e_i| \\ s.t. \  0<x_i^{min} \leq x_i \leq 1 
```

The error signal  $`e_i`$:
```math 
e_i= \bar{U}_i-U^*_i
```

### Test Cases

1. Drop Test
2. Gear Search

<img src="../hca/load_2.gif" width="600"  /><br></br>
<img src="../hca/load_3.gif" width="600"  /><br></br>
<img src="../hca/load_4.gif" width="600"  /><br></br>

**Approach 1:** Aggregated Target Sum:

```math 
 U^*_i= \sum_{j=0}^t U^*_{i,j} $$
```
```mermaid 
graph LR 
    A[start] -->B[End]
    C[0] --> D[t] -.update-.- E[0] -->F[t] -.update-.- G[0] --> H[t] -.update-.- I[0] --> J[t]
    A --> C
    J --> B

```
... or only consider final position:
```math 
 U^*_i= U^*_{i,t} $$
```

**Approach 2:** Online Search:
```math 
 U^*_i= U^*_{i,j}
```

```mermaid
graph LR
   A[start] --> 0 -.update-.- 1 -.update-.- 2 -.update-.- 3 -.update-.- J[t] -->B[End]

```




----

## TODO Next Steps
- [ ] change to triangular lattice
- [ ] Define local rule for dynamic gear search
- [ ] Drop Test
  - [ ] Repeat static with dynamic simulation
    - [ ] rewrite problem and boundary conditions
    - [ ] 2d simulation?/constrains
    - [ ] do drop test
    - [ ] optimization
      - [ ] make mass=0? or just change E?
      - [ ] bounce