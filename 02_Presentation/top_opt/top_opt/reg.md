## 1. Minimum Compliance:

Equations and detailed explanation [here.](https://amiraa.pages.cba.mit.edu/metavoxels/03_Research/toponotes3.html) (adapted from [here.](https://designinformaticslab.github.io/mechdesign_lecture/2018/04/09/topology.html))

Initial Notebook [Link](https://amiraa.pages.cba.mit.edu/metavoxels/03_Research/topologyOptimization3d.html).

### 2D

<!-- <img src="./top.png" width="300" />  -->
<img src="../top.gif" width="30%" /> <br></br>
<!-- <img src="./topcom.png" width="300" /> -->

### 3D

<img src="../top3d.gif" width="30%" /><br></br>

---

## 2. Desired Compliance:

### 2D

<img src="../topcom.gif" width="30%" /> <br></br>

### 3D

<img src="../top3dcom.gif" width="30%" />
<img src="../top3dcomsim.gif" width="30%" /><br></br>

-----