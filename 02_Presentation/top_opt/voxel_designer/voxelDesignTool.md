# Voxel Design Tool

This is automated workflow for **the inverse design of voxel units** that exhibit a desired deformation given input loads. This is the first step for automated to generate a family of voxels similar to [DM3](https://gitlab.cba.mit.edu/bej/dm3) in order to use later for larger scale optimization.

I formulated the problem as a constrained optimization problem, where, given a dense start domain of fully connected (or locally connected) truss/frame elements, the objective is to minimize the volume (sum of elements' areas) of the structure while respecting the equilibrium and desired deformation. A SIMP penalty pushes the area of members to be either be very big or very small. I derived the gradients for the objective function and the constraints and using MMA (Method of Moving Asymptotes) to do the search.

<img src="./oldConcept.png" width="80%" /> <br></br>
<img src="./newConcept.png" width="80%" /> <br></br>

---
## Constrained Optimization Formulation for Compliant Mechanisms Design

```math
\begin{aligned}
& \underset{\rho^e}{\text{minimize}}
& & V(\rho^e)=\sum_{e \in \Omega } \rho^e \upsilon^e \\
& \text{subject to}
& & K (\rho^e)d-F=0  \\
& & & g= L_i^T d\leq d_{max,i} \ \ \   \forall i \in 1,..,m \\
& & &\rho^e_{min} \leq \rho^e \ \ \ \forall \ e
\end{aligned}
```

*Using the adjoint method*:

The gradient of objective function:
<!-- $$ -->
```math
f= \sum_{e \in \Omega } \rho^e \upsilon^e \\

\frac{\delta f}{\delta \rho_e}=\sum_{e \in \Omega }  \upsilon^e 
```
<!-- $$ -->
For each constraint $i \forall (1,..,m)$:

<!-- $$ -->
```math
g_a=L_i^T d - d_{max,i} -\lambda_i^T (Kd-F) \\
\frac{\delta g_A}{\delta \rho_e}=L_i^T  \frac{\delta d}{\delta \rho_e} -\lambda_i^T(\frac{\delta K(\rho_e)}{\delta \rho_e } d +K \frac{\delta d}{\delta \rho_e}) \\
= - \lambda_i^T \frac{\delta K(\rho_e)}{\delta \rho_e }d + (L_i^T-\lambda_i^TK) \frac{\delta d}{\delta \rho_e}\\

\therefore K\lambda_i=L_i \\
\therefore   \frac{\delta g_A}{\delta \rho_e}= - \lambda_i^T \frac{\delta K(\rho_e)}{\delta \rho_e } d \\
```
<!-- $$ -->


SIMP Penalty (to push the elements to be either 0 or 1)

<!-- $$ -->
```math
K^e=((\rho^e)^\eta +\rho_{min}^e )K_0^e \\
\frac{\delta f}{\delta \rho^e}=\eta(\rho^e)^{\eta-1}K_0^e
```
<!-- $$ -->

----
## Results

### 2D Optimization

<img src="./2d.png" width="100%" /> <br></br>

### 3D Optimization

<img src="./inverter3d.png" width="40%" /> 
<img src="./inverter.gif" width="40%" /> <br></br>


<img src="./gripper3d.png" width="45%" /> 
<img src="./gripper2.gif" width="40%" /> <br></br>


<img src="./gripper3d.png" width="45%" /> 
<img src="./gripper1.gif" width="40%" /> <br></br>


### Initial Voxel Results (Truss Elements):


- Boundary Conditions and Search Domain:

<img src="./boundary.gif" width="85%" /> 

- Positive Poisson Ratio

<img src="./positive.png" width="45%" /> 
<img src="./positive_truss.gif" width="45%" /> 

- Auxetic

<img src="./auxetic_truss.gif" width="45%" /> 
<img src="./auxetic.gif" width="45%" /> 

- Chiral
  
<img src="./chiral.gif" width="45%" /> 
<img src="./chiral1.gif" width="45%" /> 

- Shear
  
<img src="./shear.gif" width="45%" /> 


Positive Frame Elements:

<img src="./positive_beam.gif" width="45%" /> 


### Next Steps
- Clean Results
- Desired Elasticity Tensor
- Take Advantage of symmetry for more refined search
- Batch Search
