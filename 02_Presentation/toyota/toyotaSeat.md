# Toyota Morphing Lattice Seat back


<img src="./img/requirement.png" width="60%" />

---

## Approaches

### [1- Alphonso's work](https://gitlab.cba.mit.edu/alfonso/toyota.-cushion-structures.)
---

### 2- Density Topology Optimization

<img src="./img/obj.png" width="100%" />
Boundary Conditions:

<img src="./img/boundary.png" width="60%" />

Topology Optimization Results:
  
<img src="./img/1.gif" width="30%" />
<img src="./img/2.gif" width="30%" />


<img src="./img/cross.png" width="60%" />
<img src="./img/attachement.png" width="60%" />

---

### 3- Microstructure Design

Find microstructure compliant in this direction

<img src="./img/Ue.png" width="40%" /> <br>

<img src="./img/1.png" width="30%" />
<img src="./img/2.png" width="30%" />
<img src="./img/3.png" width="30%" />


<img src="./img/cross2.png" width="60%" />
  
<img src="./img/one.gif" width="60%" />
<img src="./img/three.gif" width="60%" />


---

## Next Steps

- Start physical prototypes
- 3D geometries
- Explore the introduction of active components (David's distributed actuation)


---