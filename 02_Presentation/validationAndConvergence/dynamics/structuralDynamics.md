# Structural Dynamics Validation


## Slender Cantilevered Beam Validation

I first reproduced the cantilevered beam example setup by Chris [here](https://gitlab.cba.mit.edu/amiraa/metavoxels-code/-/blob/chris/dynamic_validation.md). Since a hard constraint on beam parameters is that the length/height or width ratio be > 10, he chose to use 100. Properties of the beam are as follows:

- L = 1 m
- w = h = 0.01 m
- A = 0.0001 m^2
- Iyy, Izz = w*h^4/12 m^4
- Izz = w*h^4/6 m^4
- E = 6.9e10 N/m^2
- nu = 0.33
- rho = 2700 kg/m^3
- m = 2.7 kg/m
- Force (at tip)= 20 N
- 0 global and local damping

How will we do the comparison?
- One option is to calculate the exact time solution to these unit impulsive loadings and compare pointwise through time. I added notes on how to approach this using the piecewise exact method [here.](./structuralDynamicsIntro.md)
- The other option, which may be quicker, is to extract the natural frequencies and mode shapes from the metavoxels response and comparing to analytical frequencies and mode shapes. Because MetaVoxels is matrix-free we can't solve the eigenvalue problem directly.

I will compare against the exact analytical solution and against the euler-bernoulli beam FEM code I implemented earlier.

#### Exact Solution: Predicted natural frequencies

```math
Bending   : wn = an^2 * \sqrt{EI/(mL^4)} \ \ , \ \ an^2 = 3.516, 22.035, 61.697 ... \\
Axial     : wn = n *pi/2 * \sqrt{EA/(mL^2)} \\
Torsional : wn = n *pi/2 * \sqrt{GJ/(IzzL^2)} \\
```

- Analytical Bending frequencies of the cantilever beam: $`[8.166, 51.178, 143.2967]`$

#### Euler-Bernoulli Beam FEM

I implemented earlier an Euler-Bernoulli Beam Finite Element solver (validated against Fram3DD).  My notes about the mass and stiffness matrix derivation from principle of virtual work and from the shape functions can be found [here](beamMassStiffnessMatix.md).

For a 2D Beam, the element stiffness matrix is:
```math
\begin{bmatrix} K  \end{bmatrix}= \frac{ E I}{l^3} \begin{vmatrix} 
12  & 6l  & -12& 6l \\\ 
    & 4l^2 & -6l& 2l^2\\\ 
    &     & 12 & -6l\\\ 
    &     &    & 4l^2 \end{vmatrix}
```
and the mass matrix is:
```math
\begin{bmatrix} M \end{bmatrix}= \frac{ \rho A l}{420} \begin{vmatrix} 
156  & 22l  & 54& -13l \\\ 
    & 4l^2 & 13l& -3l^2\\\ 
    &      & 156 & -22l\\\ 
    &      &    & 4l^2 \end{vmatrix}
```
After I assemble the global matrix and remove the boundary conditions, one can find the bending natural frequencies.

```math
det[[K]-\omega^2[M]]=0 \\
\omega=\sqrt{Eigenvalues}
```

- FEM Bending frequencies of the cantilever beam (10 elements): $`[8.166, 51.178, 143.333, 281.0729, 465.361]`$


### MetaVoxels Results

(Red horizontal lines are the FEM frequencies calculated in the last sections, blue lines are the fft of the x displacement of the final node).

- 40 Elements:<br></br>
<img src="./40.png" width=50%><br></br>
- 10 Elements:<br></br>
<img src="./10.png" width=50%><br></br>
- 5 Elements:<br></br>
<img src="./5.png" width=50%><br></br>
- 1 Element:<br></br>
<img src="./1.png" width=50%><br></br>



### Damping
Applying Critical damping (=1.0, local relative damping) does not seem to have an effect on the results (it just gets rid of small vibrations between neighboring nodes). 

Global Damping has a major effect on the results:

- 40 Elements without global damping:<br></br>
<img src="./40Dis.png" width=50%><br></br>
- 40 Elements with 0.0001 global damping:<br></br>
<img src="./40DisDamp.png" width=50%><br></br>
- Natural Frequency:<br></br>
<img src="./40Damp.png" width=50%><br></br>



---


## Validation against voxel beam

Chris has begun work on collecting the impulse response of a cantilevered 5x1 voxel beam. The data shown below was acquired using the microcontroller-imu setup from the voxelcopter, but the next step is setting up the laser displacement sensor for higher rate, non-contact measurement.

<img src="./voxel_dynamic.png" width=30%><br></br>

<img src="./voxel_time_series.png" width=50%><br></br>
<img src="./voxel_frequency.png" width=50%><br></br>

### MetaVoxels Results

- No damping
  
<img src="./vibvoxel.gif" width=30%><br></br>
<img src="./voxnodampdis.png" width=50%><br></br>
<img src="./voxnodamp.png" width=50%><br></br>

- damping

<img src="./voxdamp.png" width=50%><br></br>

---

## Tendons


Details about the tendon section validation against experimental results can be found [here.](./../../robotics/tendon/tendon.md)



---


## Next Steps

- Fix moments of inertia: update for large deformations based on Chris's detailed explanation [here.](https://gitlab.cba.mit.edu/amiraa/metavoxels-code/-/blob/chris/dynamic_validation.md)
- Experimentally get the damping ratio. 

---