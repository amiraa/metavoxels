# Structural Dynamics Introduction

Based on the course ["Fundamentals of Engineering Structural Dynamics with Python"](https://www.udemy.com/course/engineering-structural-dynamics-with-python).


----

## Worked Example 1:

Consider the spring-mass-damper system below. The mass is displaced vertically by 10 mm and allowed to oscillate freely. The next oscillation peak is noted to be 7.3 mm.

The system has the following parameters:

<ul>
    <li>Mass, $M = 150 \:kg$</li>    
    <li>Spring stiffness,  $k = 12\times 10^3$ N/m</li>    
</ul>

<img src="./image_1.png"></img>

Determine the following dynamic parameters:

<ol>
    <li>The natural frequency of the undamped system (in rads/sec & Hz)</li>    
    <li>The oscillation period of the undamped system</li>    
<li>The logarithmic decrement</li>    
    <li>The damping ratio</li>    
    <li>The damping coefficient</li>    
    <li>The damped natural frequency (in rads/sec)</li>    
</ol>
<hr>

``` python
m = 150 # (kg) Mass
k = 12*10**3 # (N/m) Stiffness
u1 = 10 # (mm) Peak 1
u2 = 7.3 # (mm) Peak 2
```

### 1. Natural Frequency, $`\omega_n`$ & $`f_n`$

```math
\omega_n = \sqrt{\frac{k}{m}} \\
f_n = \frac{\omega_n}{2\pi}
```

```python
# Natural frequency and period
omega_n = round(math.sqrt(k/m),3) # (rads/sec) Angular natural frequency
fn = round(omega_n/(2*math.pi),3) # (Hz) Natural frequency
```
### 2. The oscillation Period, $`T`$
```math
T=\frac{1}{f_n}
```

```python
T = round(1/fn,2)
```

### 3. Logarithmic decrement, $`\delta`$
```math
\delta = \ln{\frac{U_n}{U_{n+1}}}
```
```python
delta = round(np.log(u1/u2),3)
```

### 4. Damping ratio, $`\xi`$
```math
\xi \approx \frac{\delta}{2\pi}
```
```python
xi = round(delta/(2*math.pi),3)
```

### 5. Damping coefficient, $`c`$
```math
c = 2\xi m \omega_n
```

```python
c = round(xi*2*m*omega_n,2)
```

### 6. Damped natural frequency, $\omega_d``$


```math
\omega_d = \omega_n\sqrt{1-\xi^2}
```
```python
c = round(xi*2*m*omega_n,2)
```
```python
omega_d = round(omega_n*math.sqrt(1-(xi**2)),3)
```

1. (a) The angular natural frequency is 8.944 radians per second
1. (b) The natural frequency is 1.423 Hz
2.  The period is 0.7 seconds
3.  The logarithmic decrement is 0.315
4.  The damping ratio is 0.05 or 5.0 percent of critical damping
5.  The damping coefficient is 134.16 Ns/m
6.  The damp natural frequency is 8.933 radians per second
----



## Worked Example 2:

Consider the lightweight steel frame structure shown below. It supports heavy machinery with a total mass of $`m = 10,000\: kg`$. We can neglect the weight of the steel frame. Based on an experimental impact test, the inherent structural damping has been estimated to be 2% ($`\xi=0.02`$).

A load test has shown that a lateral force of $`P=1500\: N`$ induces a lateral displacement of $`\Delta = 7 \:mm`$.

The steel frame is constrained such that it can only move laterally (i.e. there is no twisting or significant vertical movement). As such it can be modelled as a SDoF system as shown on the right. 

<img src="./image_2.png"></img>

With this information, determine the following:
<ol>
    <li>The undamped natural frequency</li>
    <li>The damping coefficient</li>
    <li>The logarithmic decrement</li>
    <li>The damped angular natural frequency</li>
    <li>The number of cycles for the amplitude to reduce to 0.001 m</li>
    <li>Plot the free vibration response, assuming the structure was released from its initial displacement of 7 mm</li>
</ol>  

```python
# Constants
m = 10000 # (kg) Mass
xi = 0.02 # Damping ratio
P = 1500 # (N) Static force magnitude
Delta = 7 # (mm) Static displacement
```

### 1. Undamped natural frequency, $\omega_n$ & $f_n$
```python
# Undamped natural frequency
k = P/(Delta/1000) #(N/m) Stiffness
omega_n = round(math.sqrt(k/m),3) #(rads/sec) Angular natural frequency
fn = round(omega_n/(2*math.pi),3) # (Hz) Natural frequency
T = round(1/fn,2) # (sec) Period of oscillation
```

1. The angular natural frequency is 4.629 radians per second or 0.737 Hz with a period of 1.36 seconds
2. The damping coefficient is 1851.6 Ns/m
3. The logarithmic decrement is 0.126
4. The damped angular natural frequency is 4.628 radians per second

### 5. Number of oscillations before amplitude reduces to $0.001$ m

Pood that it will be logarithmic decrements:
```math
\frac{u_1}{u_n}= \frac{u_1}{u_2}*\frac{u_2}{u_3}*..\frac{u_{n-1}}{u_n} \\
ln(\frac{u_1}{u_n})=\delta+\delta+...+\delta=n \delta
```
u_start = 0.007
u_finish = 0.001
n = math.log(u_start/u_finish)/delta
n_full = math.ceil(n)
t_full = T*n_full

print("5.0 It takes {one} complete cycles and {two} seconds for the oscillation amplitude to reduce to {three} mm".format(one=n_full, two=t_full, three=u_finish))

5. It takes 16 complete cycles and 21.76 seconds for the oscillation amplitude to reduce to 0.001 mm

### 6. Free vibration response
```math
u(t) = e^{-\xi\omega_n t}[A \sin(\omega_d t) + B\cos(\omega_d t) ]
```

#### Initial conditions

```math
u(t=0) = 0.007 \:\text{m}\\
\dot{u}(t=0) = 0 \:\text{m/s}
```

Need to differentiate $`u(t)`$ to get expression for $`\dot{u}(t)`$...

```math
\dot{u}(t) = A\left[e^{-\xi\omega_nt}\omega_d\cos(\omega_dt) - \sin(\omega_dt)\: \xi\omega_n \: e^{-\xi\omega_nt}\right] + B\left[-e^{-\xi\omega_nt}\omega_d\sin(\omega_dt) - \cos(\omega_dt)\:\xi\omega_n\:e^{-\xi\omega_nt} \right]
```

Sub initial conditions into equations for $`u(t)`$ and $`\dot{u}(t)`$ to determine A and B

```math
A = 0.00014\\
B = 0.007
```

Therefore the free vibration response is given by,

```math
u(t) = e^{-\xi\omega_n t}[0.00014\: \sin(\omega_d t) + 0.007\:\cos(\omega_d t)]
```

The plot:

<img src="./sol_2.png"></img>


----

## Piecewise Exact Method

### Development
Consider a force, $`p(t)`$ (a) and its piecewise linear representation between $`t_n`$ and $`t_{n+1}`$ (c),

<img src="./piecewise.png"></img>

If the duration $`t_{n+1}-t_{n}`$ is sufficiently small, the error introduced by the linear approximation is negligibly small. The force can be represented as a function of time over the interval,

```math
p(\tau) = \overbrace{p_n}^{\text{initial value}} + \overbrace{\frac{p_{n+1}-p_n}{h}\tau}^{\text{linearly varying}}
```

The equation of motion describing the behaviour of the system is therefore,

```math
m\ddot{u} + c\dot{u} + ku = p_n + \frac{p_{n+1}-p_n}{h}\tau
```

We can determine an analytical solution to this equation of motion using superposition (remember linear analysis assumed over the duration of a timestep). The complete solution consists of three components:

- the free vibration component with initial position $u_n$ and velocity $`\dot{u}_n`$ at $`t_n`$
- the component due to a constant force of magnitude $p_n$    
- the component due to the linearly varying component of force,        
```math 
\frac{p_{n+1}-p_n}{h}\tau
```

The solution to the differential equation describing each of the three components of response can be obtained by using the same procedures demonstrated previously in this course (solving to find homogeneous and complimentary solutions to the various equations of motion). For brevity we will simply state the solutions here rather than deriving each. 

The free vibration response is given by,

```math
u_1 = e^{-\xi\omega_n\tau}\left[u_0\cos(\omega_d\tau) + \frac{\dot{u}_0 + \xi\omega_nu_0}{\omega_d}\sin(\omega_d\tau) \right]
```

The response due to constant force magnitude is given by,

```math
u_2 = \frac{p_n}{k}\left[1-e^{-\xi\omega_n\tau} \left(\cos(\omega_d\tau) + \frac{\xi\omega_n}{\omega_d} \sin(\omega_d\tau) \right) \right]
```

Finally the linearly varying force response is given by,

```math
u_3 = \frac{p_{n+1}-p_n}{k}\left[\tau - \frac{2\xi}{\omega_n} + e^{-\xi\omega_n\tau} \left(\frac{2\xi^2-1}{\omega_d}\sin(\omega_d\tau) + \frac{2\xi}{\omega_n} \cos(\omega_d\tau) \right) \right]
```

The system position at the end of a timestep can be determined by superimposing $`u_1, u_2`$ and $`u_3`$. We could determine the velocity by simply differentiating these expressions. 

In principle, with knowledge of our the system's initial position, velocity and applied force, we could 'step' our way through each timestep to determine the system position and velocity at any time in the future.

### Computational Scheme
In order to implement this concept in a simple time-stepping algorithm, we'll first restructure the equations to make them more 'algorithm friendly'. I've used the formulation presented by [Humar]. You can optionally refer to that text for more on this but everything you need to implement this technique is presented here. 

If we differentiate expressions for $`u_1, u_2`$ and $`u_3`$ we can state the following expressions for the position and velocity at the end of a timestep as,

```math
u_{n+1} = A\:u_n + B\:\dot{u}_{n} +C\:p_n + D\:p_{n+1}
```

```math
\dot{u}_{n+1} = A_1\:u_n + B_1\:\dot{u}_n + C_1\:p_n + D_1\:p_{n+1}
```

where the constants $A$ to $D_1$ are given by (take a deep breath!)...

```math
A = e^{-\xi\omega_nh}\left[\frac{\xi}{\sqrt{1-\xi^2}}\sin(\omega_dh) + \cos(\omega_dh) \right]
```

```math
B=e^{-\xi\omega_nh}\left[\frac{1}{\omega_d}\sin(\omega_dh) \right]
```

```math
C=\frac{1}{k}\left[\frac{2\xi}{\omega_nh} + e^{-\xi\omega_nh}\left(\left(\frac{1-2\xi^2}{\omega_dh} - \frac{\xi}{\sqrt{1-\xi^2}} \right)\sin(\omega_dh)-\left(1+\frac{2\xi}{\omega_n h} \right)\cos(\omega_dh)\right) \right]
```

```math
D=\frac{1}{k}\left[1-\frac{2\xi}{\omega_nh}+e^{-\xi\omega_nh}\left(\frac{2\xi^2-1}{\omega_dh}\sin(\omega_dh) + \frac{2\xi}{\omega_nh}\cos(\omega_dh) \right) \right]
```

```math
A_1=-e^{-\xi\omega_nh}\left[\frac{\omega_n}{\sqrt{1-\xi^2}}\sin(\omega_dh) \right]
```

```math
B_1=e^{-\xi\omega_nh}\left[\cos(\omega_dh)-\frac{\xi}{\sqrt{1-\xi^2}}\sin(\omega_dh) \right]
```

```math
C_1=\frac{1}{k}\left[-\frac{1}{h} + e^{-\xi\omega_nh}\left(\left(\frac{\omega_n}{\sqrt{1-\xi^2}} + \frac{\xi}{h\sqrt{1-\xi^2}} \right)\sin(\omega_dh) +\frac{1}{h}\cos(\omega_d h) \right) \right]
```

```math
D_1=\frac{1}{k}\left[\frac{1}{h} - \frac{e^{-\xi\omega_nh}}{h} \left(\frac{\xi}{\sqrt{1-\xi^2}}\sin(\omega_dh)+\cos(\omega_dh) \right)\right]
```

Notice that constants A to D1 are just that, constants. Therefore they can be calculated once at the outset of our solution and then swiftly left alone!

Although these expressions appear exceptionally complex. It's important to remember that fundamentally all we have done is implement the same procedures you are familiar with from the analysis of harmonic loading. 

It's also worth remembering that the purpose of our work here is to develop a set of equations that is suitable for implementation in a computer algorithm. As such it is not expected that you would manually process these equations to determine the step-by-step response of the structure.  

### Reference
Humar, J.L., Dynamics of Structures, 2nd Edition


---