# Validation and Convergence

## Static Convergence Studies:

<img src="./dyn_conv.gif" width="300" />

The graphs below show that given enough time steps (because of damping and propagation effects) the dynamic model converges to the linear FEA model.


![](./FINAL_VAL.png)

The three graphs below show the final node displacement in the x,y,z directions for the the 7*7*7 lattice. One can see how accurate the simulation is for all 1400 nodes.

![](./x3.png)
![](./y3.png)
![](./z3.png)

<!-- ![](./total_convergence1.png)

![](./node_displacement_four_voxel.png) -->

The convergence rate is based on three physical parameters: the size (depth) of the structure, the youngs modulus (the more stiff the faster it converges) and the mass of the nodes. That's because, in order to ensure stability, the time step and damping factor calculation are based on the youngs modulus and node mass  [(see the structural mechanics models explanation)](https://amiraa.pages.cba.mit.edu/metavoxels/03_Research/structureModel.html). Below is the same voxel structure with a much higher edge stiffness which increases the damping force and makes it converge much faster.

<!-- ![](./dynamic_convergence.png) -->

Even though a large amount of timesteps is needed for the convergence, the simulation speed is still orders of magnitude faster than the FEA analysis for given that for large structures. That's because:

1- The dynamic model does need not assemble or invert a large stiffness matrix for the displacement calculation, which is usually the bottleneck of all fea solvers as these steps are very hard to parallelize (there are ways to parallelize them using graph coloring techniques but still very inefficient for non sparse structures)

2- All the calculations are hence highly parallelized and one is able to use the gpu for computation

3- the bottleneck in the gpu computation is  getting the arrays from the cpu to the gpu, but once it's there, adding more time steps costs basically nothing

4- now that the dynamic model is working, it's very easy to incorporate non linear deformation/analysis 

### Youngs modulus:

The youngs modulus is the ration between the stress and strain in the material/structure. 

<!-- $$ -->

```math
E=\frac{\sigma}{\epsilon} = \frac{F/A}{\Delta l/l_0}
```
<!-- $$ -->

By adding a fixed stress (force over area) to the voxel structures with different lattice sizes, one can see that the youngs modulus (stiffness) increases with the lattice size. 

The youngs modulus of the converged dynamic model and static fea model are perfectly the same.


![](./youngs_modulus1.png)

### Validation and Comparison against Experimental data:

In order to validate the simulation results, I compared it to [voxframe](https://gitlab.cba.mit.edu/ccameron/voxframe/-/tree/master) simulation results (based on [Frame3DD](http://frame3dd.sourceforge.net/)).My results perfectly fit the voxframe results given the same physical parameters and loading conditions.

They both were though modeled using a single beam instead of double beams like in real life, that's why there is a discrepancy between the tested youngs modulus and the simulation.

![](./youngs_fea_comparison.png)


-------
## Structural Dynamics Validation


More Details [here.](dynamics/structuralDynamics.md)


---
