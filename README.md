<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_CHTML"></script>

# MetaVoxels

Tools for the design and simulation of metavoxels (meta-material voxel lattice structures). The objective is the inverse design of metavoxels using the base voxels studied in [DM3](https://gitlab.cba.mit.edu/bej/dm3).

I am now using this gitlab project for documentation only, the code lives [here](https://gitlab.cba.mit.edu/amiraa/metavoxels-code) now.

<img src="02_Presentation/hierarchy.png" width="80%" /><br></br>
<img src="02_Presentation/overall.gif" width="80%" /><br></br>

<!-- <img src="02_Presentation/simpleBeam.mp4" width="40%" /><br></br>
<img src="02_Presentation/hier.mp4" width="40%" /> <br></br>
<img src="02_Presentation/5BaseVoxel.gif" width="40%" /><br></br>
<img src="02_Presentation/chi2.gif" width="40%" /><br></br>
<img src="02_Presentation/chi1.gif" width="40%" /><br></br> -->


-------------

## Code
<!-- - [Group](https://gitlab.cba.mit.edu/groups/metavoxel/-/shared) -->
- [Code Project](https://gitlab.cba.mit.edu/amiraa/metavoxels-code): https://gitlab.cba.mit.edu/amiraa/metavoxels-code
- [Installation and Code Structure](02_Presentation/documentation/codeStructure.md)
  - [Tutorial](https://gitlab.cba.mit.edu/amiraa/metavoxels-code/-/blob/master/jupyter/MetaVoxel_Tutorial.ipynb)
- [GPU Parallelization and Scaling](02_Presentation/performance/performance.md)

---

## Physics
- [Structural Mechanics Model](03_Research/structureModel.md)
- [Validation and Convergence](02_Presentation/validationAndConvergence/validationAndConvergenceStudies.md)
- [Non-linear Deformation and Thermal Expansion](02_Presentation/physics/physics.md)
- [Hierarchy](02_Presentation/hierarchy/hierarchy.md)
- [Fluid Solid Interaction](02_Presentation/physics/lbm/lbm.md)


---

## Search
- [Inverse Design of Discrete Mechanical Metamaterial](https://gitlab.cba.mit.edu/amiraa/inverse-design-of-discrete-mechanical-metamaterial/-/blob/main/documentation.md)
- [Topology Optimization](02_Presentation/top_opt/search.md)
  - [Hybrid Cellular Automata](https://amiraa.pages.cba.mit.edu/cpp_frep/hca.html)
- [Voxel Design Tool](02_Presentation/top_opt/voxel_designer/voxelDesignTool.md)
  - [Inverse Homogenization](02_Presentation/top_op/../top_opt/inverse_hom/inverse_homogenization.md)
  - [Multiscale topology optimization](https://amiraa.pages.cba.mit.edu/presentations/201203_Topology_Optimization/index.html#/)
- [Mind Body Problem](./02_Presentation/mindBody/mindBody.md)
- [Toyota Seat](./02_Presentation/toyota/toyotaSeat.md)

---

## Robotics
- [Piano-horse (Drop Test)](02_Presentation/drop/dropTest.md)
- [Rover](02_Presentation/robotics/rover/rover.md)
- [Tendon](02_Presentation/robotics/tendon/tendon.md)
  - [Hydorsnake (old)](02_Presentation/robotics/snake/snake.md)
  - [Walker](02_Presentation/robotics/walking/walking.md)
    - [Puppy](02_Presentation/robotics/walking/puppy.md)
- [Morphing Wing](02_Presentation/robotics/wing/morphingWing.md)
----

## Assembly
- [Assembler Load](02_Presentation/assembly/assemblyLoad.md)
- [Swarm Assembly](https://amiraa.pages.cba.mit.edu/swarm-morphing/Bill-e_swarm_simulator/index.html)
- [Recursive Assembly](https://amiraa.pages.cba.mit.edu/swarm-morphing/recursive_swarm/indexCone.html)

---

## Interactive Demos 

- [Tendons Modeling](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexTendon.html)
- [Rover](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexRover.html)
- [Precomputed 5*5 cuboct Voxel](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexScaling.html)
- [Precomputed 4*1 chiral Voxel](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexChiral.html)
- [Boundary Conditions](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexBoundaryConditions.html)
- [Hierarchal multi-material 3*3 Base Voxel (old cpu version (to fix))](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexHierarchical.html)
<!-- - [Precomputed 2*2 Chiral Voxel](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191115_NodeJsJulia/demos/indexChiral.html)
- [Precomputed 3*3 Chiral Voxel](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191115_NodeJsJulia/demos/indexChiral3.html)
- [(Computation & visualization) 5*5 Base Voxel](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191115_NodeJsJulia/demos/indexParallel.html)
- [Old Demo chiral  voxel (Computation & visualization)](https://amiraa.pages.cba.mit.edu/metavoxels/01_Code/191104_MetaVoxel/index.html) -->


----
# TODO

## Desired Milestones
- [x] 3D visualization tool
- [x] One voxel simulation
- [x] Multi-voxel simulation
- [x] Physical simulation comparison
- [x] Parallel dynamic ODE solver
- [x] Hierarchical simulation
- [x] One voxel optimization based on desired deformation
- [x] Inverse Design of Large Structures
- [ ] Inverse Design of Active structures (offline and online)
- [ ] Growing and Evolving structures (Path planing + simulation)
  
-----------------------
### General
  - [x] fix pipeline issue
    - [x] create group for metavoxel
    - [x] port the code to different project
  - [ ] Merge metavoxel with dice stuff??
  - [x] Debug code to use latest julia and CUDA pkg
  - [x] clean the files and put app.js and app1.js and julia notebooks in folders
### Documentation
- [x] get the video you created and add it instead of gifs
  - [x] make sure the codec information is correct
- [x] Document cpu version and how to change the number of threads
### Demos
  - [x] different kinds of voxels
    - [x] add chiral voxel
    - [x] add auxetic voxel
    - [x] add compliant voxel
    - [x] search for one way bendy voxel
    - [x] Demo and .gif different types of voxels beside each other
  - [ ] fill mesh/frep with voxels
    - [ ] port swarm paper asd (voxelizer)
      - [ ] parallelize it
    - [x] fill frep
      - [ ] get the gpu thing and get data from function
  - [x] fix linked demos
  - [x] populate interactive demos
  - [ ] drop down examples from html instead of saving lots of html pages
### Scaling and Hierarchy
  - [x] comparison hierarchical vs detailed
    - [ ] madcat
  - [x] automatic elasticity vector calculation
  - [x] use parent data to get hierarchy information
  - [x] different scale voxels in the same simulation (multi-scale simulation)
### Interface and visualization
  - [ ] change export settings instead of lost of json to only one and see performance
  - [ ] change visualization to GPU to be faster for bigger systems
    - [x] graph based
      - [ ] show the labels
    - [ ] gpu based
      - [ ] finish implementation
      - [ ] fixed max num of timesteps
  - [ ] see way to show current timestep
  - [ ] slider down for displacement instead of animation
  - [ ] gui for boundary conditions
    - [x] basic boxes
    - [ ] export to json
    - [x] CMODS
      - [x] finish implementation
      - [ ] [node from the browser](https://hackernoon.com/how-to-run-nodejs-in-a-browser-wc4s32by)
  - [x] show force function between nodes
    - [x] force graph
  - [x] show control graph
  - [ ] change to white background
### Differentiation and Search
  - [x] Redo the microstructure topology optimization
  - [x] use the compliant one to search for one way bendy structure
  - [ ] differentiation
    - [ ] inverse matrix forward methods/parameter estimation
    - [ ] [adjoint linear solve](https://mitmath.github.io/18337/lecture11/adjoints)
    - [ ] ensemble methods
    - [ ] [other](http://apps.amandaghassaei.com/TrussOptimization2D/)
### Physics 
  - [x] change recommended time-step to look at all lengths, E and areas
  - [x] Non linear behavior get the stress strain curve multiple points and use them
  - [ ] add collision/bins
  - [ ] add note about units
  - [ ] add detailed parametric study about the different values that can be changed and effect on convergence and num time steps
    - [ ] for ex: damping factor, stiffness, mass, time steps
  - [ ] Explore double beams 
  - [ ] Add definitions for other beam cross sections
  - [ ] include other integrators (better than verlet integration)
   

---------------------
  

